'use strict';

// to enable these logs set `DEBUG=boot:01-load-settings` or `DEBUG=boot:*`
var log = require('debug')('boot:01-load-settings');

module.exports = function(app) {

  var Setting = app.models.Setting;

  function loadDefaultSettings() {
    console.error('Creating default settings');

    var settings = [{
      key: 'appName',
      value: 'Dfood'
    }, {
      key: 'priceDelevery',
      value: 3500,
    }];

    settings.forEach(function(setting) {
      Setting.create(setting, function(err) {
        if (err) {
          console.error(err);
        }
      });
    });
  }

  function loadExistingSettings() {
    console.error('Loading existing settings');

    Setting.find(function(data) {
      log(data);
    });
  }


  Setting.count(function(err, result) {
    if (err) {
      console.error(err);
    }
    if (result < 1) {
      loadDefaultSettings();
    } else {
      loadExistingSettings();
    }
  });


};
