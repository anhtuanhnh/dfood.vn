'use strict';

// to enable these logs set `DEBUG=boot:03-load-content` or `DEBUG=boot:*`
var log = require('debug')('boot:03-load-content');

module.exports = function (app) {

  if (app.dataSources.db.name !== 'Memory' && !process.env.INITDB) {
    return;
  }

  log('Creating categories and products');

  var Category = app.models.Category;
  var Product = app.models.Product;

  var d = new Date().toDateString();
  Category.findOrCreate(
    {where: {name: 'Hương Việt'}}, // find
    {
      created: d,
      name: 'Hương Việt',
      zone: 'Huế',
      address: '31 Lê Duẫn, Huế',
      type: 'Nhà hàng',
      close: '21:00',
      open: '8:00',
      common: 1,
      imageSrc: 'http://muaban.ru/FileUpload/Images/301211_085737logo_huong_viet_doi_mau_copy.jpg',
      ownerId: 3
    }, // create
    function (err, category, created) {
      if (err) {
        console.error('err', err);
      }
      (created) ? log('created Category', category.name)
        : log('found Category', category.name);
      Product.findOrCreate(
        {where: {name: 'Cơm hến'}}, // find
        {
          created: d,
          name: 'Cơm hến',
          introduce: 'Được nấu từ gạo nết thơm ngon, hến tươi được chế biến kỹ lưỡng với công thức gia truyền',
          type: 'Món mặn',
          unit: 'Suất',
          timeProcess: '30 phút',
          promotion: 'Tặng cơm thêm miễn phí',
          price: 50000,
          categoryId: category.id,
          categoryName: category.name
        }, // create
        function (err, data, created) {
          if (err) {
            console.error('err', err);
          }
          (created) ? log('created Product', data.name)
            : log('found Product', data.name);
        });
      Product.findOrCreate(
        {where: {name: 'Chè cung đình'}}, // find
        {
          created: d,
          name: 'Chè cung đình',
          introduce: 'Gà ta',
          type: 'Món ngọt',
          unit: 'Ly',
          timeProcess: '30 phút',
          promotion: 'Mua 2 tặng 1',
          price: 15000,
          categoryId: category.id,
          categoryName: category.name
        }, //create
        function (err, data, created) {
          if (err) {
            console.error('err', err);
          }
          (created) ? log('created Product', data.name)
            : log('found Product', data.name);
        });
    });

  Category.findOrCreate({where: {name: 'Phì Lũ'}}, {
    created: d,
    name: 'Phì Lũ',
    zone: 'Đà Nẵng',
    address: '31 Điện Biên Phủ, Đà Nẵng',
    type: 'Nhà hàng',
    close: '21:00',
    open: '8:00',
    common: 2,
    imageSrc: 'http://philu.com.vn/images/news/philu_new.jpg',
    ownerId: 3
  }, function (err, category, created) {
    if (err) {
      console.error('err', err);
    }
    (created) ? log('created Category', category.name)
      : log('found Category', category.name);
    Product.findOrCreate({where: {name: 'Cơm Trung Hoa'}}, {
      created: d,
      name: 'Cơm Trung Hoa',
      introduce: 'Được nấu từ gạo nết thơm ngon, hến tươi được chế biến kỹ lưỡng với công thức gia truyền',
      type: 'Món mặn',
      unit: 'Suất',
      timeProcess: '30 phút',
      promotion: 'Nước uống',
      price: 250000,
      categoryId: category.id,
      categoryName: category.name
    }, function (err, data, created) {
      if (err) {
        console.error('err', err);
      }
      (created) ? log('created Product', data.name)
        : log('found Product', data.name);
    });
    Product.findOrCreate({where: {name: 'White wine'}}, {
      created: d,
      name: 'Bún bò',
      introduce: 'Nước lều với công thức gia truyền',
      type: 'Món mặn',
      unit: 'Suất',
      timeProcess: '30 phút',
      promotion: 'Nước uống',
      price: 70000,
      categoryId: category.id,
      categoryName: category.name
    }, function (err, data, created) {
      if (err) {
        console.error('err', err);
      }
      (created) ? log('created Product', data.name)
        : log('found Product', data.name);
    });
  });

};
