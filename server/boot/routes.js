var loopback = require('loopback');
module.exports = function(app) {

  app.use(loopback.static(require('path').join(__dirname, '..', '../dist')));
  app.get('/', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/cart', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/sitemap.xml', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/sitemap.xml'))
  });
  app.get('/nganluong_5b80b220003e9782815340092abf52f0.html', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/nganluong_5b80b220003e9782815340092abf52f0.html' +
      ''))
  });
  app.get('/google54cf9dffeff18796.html', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/google54cf9dffeff18796.html' +
      ''))
  });
  app.get('/BingSiteAuth.xml', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/google54cf9dffeff18796.html' +
      ''))
  });
  app.get('/booking', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/payment', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/restaurants/*', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/restaurants', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/detail', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/policy', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/regulation', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/submitDriver', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/submitRestaurant', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/listRestaurants', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/about', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/detail/*', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/login', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/register', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/app', function(req, res) {
    console.log(res);
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
  app.get('/app/*', function(req, res) {
    res.sendFile(require('path').join(__dirname + '/../../dist/index.html'))
  });
};
