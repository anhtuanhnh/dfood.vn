var geocoder;
var map;
var infowindow;

function initialize() {
  // Setup the map
  geocoder = new google.maps.Geocoder();

  var latlng = new google.maps.LatLng(-34.397, 150.644);

  var mapOptions = {
    zoom: 5,
    center: latlng
  }

  // Create the map
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  // Get the data and set the marker bounds
  var markersLocations = $('#map-canvas').data('locations'),
    markerBounds = new google.maps.LatLngBounds();

  // Loop through the data getting the address
  $.each(markersLocations,function(index,Element,contentString){
    geocoder.geocode( { 'address': Element.address}, function(results, status) {

      var contentString = '<div class="noscrollbar"><h3>' + Element.cafe + '</h3><p>' + Element.address + '<br><a href="http://www.' + Element.web + '">' + Element.web +'</a></p></div>';

      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        // Add the markers
        marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          icon: 'http://maps.google.com/mapfiles/ms/icons/pink-dot.png'
        });

        if (Element.home) {
          // Change the marker of a specific marker
          marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');
          // Open the home info mark straight away
          infowindow = new google.maps.InfoWindow({
            content: contentString
          });
          infowindow.open(map, marker);
        }
        // Use the results to set the bounds
        markerBounds.extend(results[0].geometry.location);
        map.fitBounds(markerBounds);
        // Add a info window on click: add the address as the info content
        addInfoWindow(marker,contentString);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }

    });
  });
}

// Add a info window on click: add the address as the info content
function addInfoWindow(marker,contentString) {
  // Open only one infowindow on click
  google.maps.event.addListener(marker, 'click', function () {
    if (!infowindow) {
      infowindow = new google.maps.InfoWindow();
    }
    //Setting content of infowindow
    infowindow.setContent(contentString);
    // Open infowindow
    infowindow.open(map, marker);
  });
}

$(window).load(function() {
  initialize();
});
