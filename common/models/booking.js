'use strict';

module.exports = function (Booking) {

Booking.search = function (msg, cb) {

    Category.find('categories',1,function (categories) {
      console.log(categories);
      var result = categories.filter(function (item,index,array) {
        console.log(item,index,array);
        if(item.name.indexOf(msg) != -1) return {name: item.name, id: item.id};
      })
    });
    cb(null, result);
  };

  Booking.remoteMethod('search',  {
    accepts: {arg: 'msg', type: 'string',description: 'key words'},
    description: 'Search info categories',
    http: {path: '/search', verb: 'get'}, // name endpoint
    notes: [
      "Search is feature for users, who want search info about to restaurants and foods",
      "The feature is created by tuanhoang",
      "Thanks"
    ],
    returns: {arg: 'data', type: 'array'}
  })
};
