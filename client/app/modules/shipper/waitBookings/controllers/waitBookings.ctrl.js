(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.waitBookings')
    .controller('WaitBookingsCtrl', function ($scope, $rootScope, Booking,Category,CoreService,gettextCatalog) {
      var user = $rootScope.currentUser;
      console.log(user);
      $scope.bookings = {};
      function getBooking() {
        Booking.find({
          filter:{
            where:{
              status: 'Chưa giao hàng',
              zone: user.zone
            }
          }
        }, function (res) {
          $scope.bookings = res;
          $scope.bookings.forEach(function (item) {
            Category.findById({
              id: item.categoryId
            },function (value) {
              item.categoryAddress = value.address;
              item.categoryName = value.name
            })
          })


        });
      }
      getBooking();
      $scope.acceptDelivery = function (booking) {
        booking.shipperId = user.id;
        booking.shipperName = user.username;
        booking.status = 'Đang giao hàng';
        booking.deliveryTime = new Date().toDateString();
        CoreService.confirm(
          gettextCatalog.getString('Xác nhận'),
          gettextCatalog.getString('Bạn muốn gửi yêu cầu vận chuyển đơn hàng này'),
          function () {
            Booking.upsert(booking).$promise
              .then(function (result) {
                console.log(result);
                getBooking();
                CoreService.toastSuccess(
                  gettextCatalog.getString('Bạn đã đồng ý giao đơn hàng nay'),
                  gettextCatalog.getString('Chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất')
                );
              })
              .catch(function (err) {
                  CoreService.toastSuccess(
                    gettextCatalog.getString('Yêu cầu giao hàng bị lỗi'),
                    gettextCatalog.getString('Đã có lỗi xảy ra trong quá trình gửi yêu cầu ')
                  );
                }
              );
          },
          function () {
            CoreService.toastWarning(
              gettextCatalog.getString('Yêu cầu giao hàng bị hủy'),
              gettextCatalog.getString('Hãy kiểm tra lại đơn hàng')
            );
          }
        );
      };
    });
})();
