(function () {
  'use strict';
  angular
    .module('com.module.waitBookings')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.waitBookings', {
          abstract: true,
          url: '/waitBookings',
          templateUrl: 'modules/shipper/waitBookings/views/main.html'
        })
        .state('app.waitBookings.list', {
          url: '',
          templateUrl: 'modules/shipper/waitBookings/views/list.html',
          controller: 'WaitBookingsCtrl'
        })
        .state('app.waitBookings.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/shipper/waitBookings/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, WaitBookingsService, booking) {
            console.log(booking);
            this.booking = booking;
            this.formFields = WaitBookingsService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              WaitBookingsService.upsertBooking(this.booking).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            booking: function ($stateParams, WaitBookingsService) {
              return WaitBookingsService.getBooking($stateParams.id);
            }
          }
        })
        .state('app.waitBookings.view', {
          url: '/:id',
          templateUrl: 'modules/shipper/waitBookings/views/view.html',
          controllerAs: 'ctrl',
          controller: function (booking,WaitBookingsService,$localStorage) {
            this.booking = booking;
            this.user = $localStorage.user;
            console.log(booking,this.user);
            this.editFoods = function (index,operator,value) {
              if(operator =='plus'){
                this.booking.foods[index].count += 1;
                totalPriceFuc();
              } else if (operator == 'subtract'){
                if(value == 0){
                  console.log('Khong con gi de tru')
                } else {
                  this.booking.foods[index].count -= 1;
                  totalPriceFuc();
                }
              }
            }
             var totalPriceFuc = function () {
               this.booking.totalPrices = 0;
               this.booking.foods.forEach(function (item) {
                 this.booking.totalPrices += item.price * item.count
              }.bind(this));
               WaitBookingsService.upsertBooking(this.booking)
            }.bind(this)
          },
          resolve: {
            booking: function ($stateParams, WaitBookingsService) {
              return WaitBookingsService.getBooking($stateParams.id);
            }
          }
        })
    }
  );

})();
