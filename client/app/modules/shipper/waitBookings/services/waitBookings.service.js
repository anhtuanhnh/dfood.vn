(function () {
  'use strict';
  angular
    .module('com.module.bookings')
    .service('WaitBookingsService', function (CoreService, Booking, gettextCatalog,User) {
      this.getBookings = function (zone) {
        return Booking.find({
          filter: {
            where:{
              zone: zone
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBookingById = function (id) {
        return Booking.find({
          filter: {
            where:{
              userId: id
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBooking = function (id) {
        return Booking.findById({
          id: id
        }).$promise;
      };



      this.getFormFields = function () {
        return [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên đơn hàng'),
              required: true
            }
          },

          {
            key: 'email',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Email')
            }
          },
          {
            key: 'addressCustomer',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Địa chỉ giao hàng')
            }
          },
          {
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại')
            }
          },
          {
            key: 'note',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Lưu ý')
            }
          },
          {
            key: 'dateDelivery',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Ngày vận chuyển')
            }
          }
        ];
      };

    });

})();
