(function () {
  'use strict';
  angular
    .module('com.module.bookings')
    .service('ShipperBookingsService', function (CoreService, Booking, gettextCatalog,User) {
      this.getBookings = function (zone) {
        return Booking.find({
          filter: {
            where:{
              zone: zone
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBookingById = function (id) {
        return Booking.find({
          filter: {
            where:{
              userId: id
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBookingByStatus = function (status) {
        return Booking.find({
          filter: {
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBooking = function (id) {
        return Booking.findById({
          id: id
        }).$promise;
      };
    });

})();
