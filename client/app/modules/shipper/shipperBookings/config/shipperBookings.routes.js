(function () {
  'use strict';
  angular
    .module('com.module.shipperBookings')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.shipperBookings', {
          abstract: true,
          url: '/shipperBookings',
          templateUrl: 'modules/shipper/shipperBookings/views/main.html'
        })
        .state('app.shipperBookings.list', {
          url: '',
          templateUrl: 'modules/shipper/shipperBookings/views/list.html',
          controller: 'ShipperBookingCtrl'
        })
        .state('app.shipperBookings.view', {
          url: '/:id',
          templateUrl: 'modules/shipper/shipperBookings/views/view.html',
          controllerAs: 'ctrl',
          controller: function (booking) {
            this.booking = booking;
          },
          resolve: {
            booking: function ($stateParams, ShipperBookingsService) {
              return ShipperBookingsService.getBooking($stateParams.id);
            }
          }
        })

    }
  );

})();
