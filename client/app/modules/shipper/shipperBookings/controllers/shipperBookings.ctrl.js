(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.bookings')
    .controller('ShipperBookingCtrl', function ($scope, $rootScope, Booking,CoreService,gettextCatalog) {
      var user = $rootScope.currentUser;
      $scope.bookings = {};
      Booking.find({
        filter:{
          where: {
            or: [
              { and: [{ status: 'Đang giao hàng' }, { shipperId: user.id }] },
              { and: [{ status: 'Hoàn thành' }, { shipperId: user.id }] }
            ]
          }
        }
      }, function (res) {
        console.log(res);
        $scope.bookings = res;
      });

        $scope.finalBooking = function (id) {
          CoreService.confirm(
            gettextCatalog.getString('Xác nhận'),
            gettextCatalog.getString('Bạn muốn gửi yêu cầu hoàn thành đơn hàng này'),
            function () {
              $scope.bookings[id].finalTime = new Date();
              $scope.bookings[id].status = 'Hoàn thành';
              Booking.upsert($scope.bookings[id]).$promise
                .then(function (result) {
                  console.log(result);
                  CoreService.toastSuccess(
                    gettextCatalog.getString('Bạn đã gửi yêu cầu hoàn th ànhđơn hàng'),
                    gettextCatalog.getString('Chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất')
                  );
                })
                .catch(function (err) {
                    CoreService.toastSuccess(
                      gettextCatalog.getString('Yêu cầu hoàn thành bị lỗi'),
                      gettextCatalog.getString('Đã có lỗi xảy ra trong quá trình gửi yêu cầu ')
                    );
                  }
                );
            },
            function () {
              CoreService.toastWarning(
                gettextCatalog.getString('Yêu cầu giao hàng bị hủy'),
                gettextCatalog.getString('Hãy kiểm tra lại đơn hàng')
              );
            }
          );

}
    });

})();
