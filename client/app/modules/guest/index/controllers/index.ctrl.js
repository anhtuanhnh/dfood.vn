'use strict';
angular
  .module('com.module.index')
  .controller('index', function ($scope,$rootScope,$modal,Category,$localStorage,$state, ApiService, User,Product) {
    var dataCategory = [];
    var dataProduct = [];
    var userId = localStorage.getItem('$LoopBack$currentUserId');
    if(userId){
      User.findById({id: userId}, function (res) {
        $rootScope.currentUser = res;
      });
    } else {
      $rootScope.currentUser = {}
    }
    $scope.typeFood = 1;



    Category.find({
      filter:{
        fields: ['name', 'id']
      }
    }, function(value){
      dataCategory = value;
    });

    Product.find({
      filter:{
        fields: ['name', 'id', 'categoryId']
      }
    }, function(value){
      dataProduct = value;
    });
    $scope.searchCategories = [];
    $scope.searchProducts = [];
    $scope.searchFeature = function (msg) {

      $scope.searchCategories = dataCategory.filter(function (item,index,array) {
        return (item.name.toLowerCase().indexOf(msg.toLowerCase()) != -1)
      });
      $scope.searchProducts = dataProduct.filter(function (item,index,array) {
        return (item.name.toLowerCase().indexOf(msg.toLowerCase()) != -1)
      });
    };

    Category.find({
      filter:{
        limit: '8'
      }
    },function (result) {
      $scope.restaurants = result;
    });
    User.count(function (item) {
      $scope.countUser = item.count;
    });
    $scope.moveListRestaurants = function (value) {
      $state.go('index.listRestaurants', {value: value})
    };

    $scope.showMenu = function (route) {
      $('.main-menu').toggleClass('show');
    };
    $scope.closeMenu = function (route) {
      if(route){
        $state.go(route)
      }
      $('.main-menu').removeClass('show');
    };
    if(!$localStorage.foodCart){
      $localStorage.foodCart = {
        foods: []
      }
    }

    $(window).scroll(function(){
      if($(document).scrollTop() > 150)
      {
        $('.headerStyle').addClass('sticky');
      }
      else
      {
        $('.headerStyle').removeClass('sticky');
      }

    });
  }) ;

