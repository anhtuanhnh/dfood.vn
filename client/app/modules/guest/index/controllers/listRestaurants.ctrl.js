'use strict';
angular
  .module('com.module.index')
  .controller('listRestaurants', function ($scope,$localStorage,$rootScope,$stateParams,Product,Category,bookingService) {
    $scope.restaurants = [];
    $scope.foods = [];
    $scope.typeName = $stateParams.type;
    $scope.idName = $stateParams.id;
    $scope.zoneName = 'Tất cả';
    Category.count(function (number) {
      $scope.countCategory = number.count
    }) ;
    Product.find({
      filter:{ fields: ['id']}
    },function (number) {
      $scope.countProduct = number.length
    }) ;
    function queryData() {
      if($scope.typeName == 'Nha hang'){
        if($scope.idName == 0){
          Category.find(function (result) {
            $scope.restaurants = result
          });
        } else {
          Category.find({
            filter:{
              where:{
                id: $scope.idName
              }
            }
          },function (result) {
            $scope.restaurants = result
          })
        }
      } else if($scope.typeName == 'Mon an') {
        if($scope.idName == 0){
          Product.find({
            filter:{
              include: 'category'
            }
          },function (result) {
            console.log(result);
            $scope.foods = result
          });
        } else {
          Product.find({
            filter:{
              include: 'category',
              where:{
                id: $scope.idName
              }
            }
          }, function (result) {
            $scope.foods = result
          })
        }
      }
    }
    queryData();
    $scope.optionTypeSearch =function (type) {
      $scope.typeName = type;
      $scope.idName = 0;
      queryData()
    };
    $scope.optionZoneSearch = function(zone){
      $scope.zoneName = zone;
      $localStorage.zone = zone;
      if(zone == ''){
        Category.find(function (result) {
          console.log(result);
          $scope.restaurants = result
        });
      } else {
        Category.find({
          filter:{
            where:{
              zone: zone
            }
          }
        },function (result) {
          console.log(result);
          $scope.restaurants = result
        });
      }
    };
    $scope.addFood = function (food) {
      console.log(food);
      $scope.foodCart = bookingService.addFood(food,food.category.address,food.category.lat,food.category.lng,food.category.id,food.category.isDelivery);
      $scope.totalPrices = bookingService.totalPrices();
    };
  });

