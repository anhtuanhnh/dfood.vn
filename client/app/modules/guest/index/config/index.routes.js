'use strict';
angular
  .module('com.module.index')
  .config(function($stateProvider,$locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
      .state('index', {
        abstract: true,
        url: '/',
        templateUrl: 'modules/guest/index/views/index.html',
        controller: 'index'
      })
      .state('index.login', {
        url: 'login',
        templateUrl: 'modules/common/users/views/login.html' ,
        controller: 'LoginCtrl',
        params: {
          'url': '/app'
        }
      })
      .state('index.register', {
        url: 'register',
        templateUrl: 'modules/common/users/views/register.html',
        controller: 'RegisterCtrl'
      })
      .state('index.home', {
        url: '',
        templateUrl: 'modules/guest/index/views/index_home.html',
        controller: 'index'
      })
      .state('index.listRestaurants', {
        url: 'restaurants',
        templateUrl: 'modules/guest/index/views/listRestaurants/listRestaurants.html',
        controller: 'listRestaurants',
        params: {
          'type': 'Nha hang',
          'id': 0
        }
      })
      .state('index.regulation', {
        url: 'regulation',
        templateUrl: 'modules/guest/index/views/regulation/regulation.html'
      })
      .state('index.resolveComplaints', {
      url: 'resolve-complaints',
      templateUrl: 'modules/guest/index/views/resolveComplaints/resolveComplaints.html'
    })
      .state('index.about', {
        url: 'about',
        templateUrl: 'modules/guest/index/views/about/about.html'
      })
    .state('index.policy', {
      url: 'policy',
      templateUrl: 'modules/guest/index/views/policy/policy.html'
    });
  });
