'use strict';
angular
  .module('com.module.foodCarts')
  .config(function($stateProvider,$locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
      .state('foodCarts', {
        url: '/carts',
        views: {
          '': {
            templateUrl: 'modules/guest/foodCarts/views/foodCarts/foodCarts.html',
            controller: 'foodCartsCtrl'
          },
          'foodCarts_header@foodCarts': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'
          },
          'foodCarts_info@foodCarts': {
            templateUrl: 'modules/guest/foodCarts/views/foodCarts/foodCarts_info.html'
          },
          'foodCarts_main@foodCarts': {
            templateUrl: 'modules/guest/foodCarts/views/foodCarts/foodCarts_main.html',
            controller: 'foodCartsCtrl'
          },
          'foodCarts_footer@foodCarts': {
            templateUrl: 'modules/guest/index/views/index_footer.html'
          }
        },
        params: {
          'data': {},
          'price': 0
        }
      })
      .state('bookingFood', {
        url: '/booking',
        views: {
          '': {
            templateUrl: 'modules/guest/foodCarts/views/booking/bookingFood.html',
            controller: 'bookingFood'
          },
          'bookingFood_header@bookingFood': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'
          },
          'bookingFood_info@bookingFood': {
            templateUrl: 'modules/guest/foodCarts/views/booking/bookingFood_info.html'
          },
          'bookingFood_main@bookingFood': {
            templateUrl: 'modules/guest/foodCarts/views/booking/bookingFood_main.html',
            controller: 'bookingFood'
          },
          'bookingFood_footer@bookingFood': {
            templateUrl: 'modules/guest/index/views/index_footer.html'
          }
        },
        params: {
          'data': {},
          'price': 0
        }
      })
      .state('payment', {
        url: '/payment',
        views: {
          '': {
            templateUrl: 'modules/guest/foodCarts/views/payment/payment.html',
            controller: 'paymentCtrl'
          },
          'payment_header@payment': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'

          },
          'payment_info@payment': {
            templateUrl: 'modules/guest/foodCarts/views/payment/payment_info.html'
          },
          'payment_main@payment': {
            templateUrl: 'modules/guest/foodCarts/views/payment/payment_main.html'
          },
          'payment_footer@payment': {
            templateUrl: 'modules/guest/index/views/index_footer.html'
          }
        }
      })
      .state('confirm', {
        url: '/confirm',
        views: {
          '': {
            templateUrl: 'modules/guest/foodCarts/views/confirm/confirm.html',
            controller: 'paymentCtrl'
          },
          'confirm_header@confirm': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'
          },
          'confirm_info@confirm': {
            templateUrl: 'modules/guest/foodCarts/views/confirm/confirm_info.html'
          },
          'confirm_main@confirm': {
            templateUrl: 'modules/guest/foodCarts/views/confirm/confirm_main.html'
          },
          'confirm_footer@confirm': {
            templateUrl: 'modules/guest/index/views/index_footer.html'
          }
        }
      })

  });
