'use strict';
angular
  .module('com.module.foodCarts')
  .controller('foodCartsCtrl',function ($scope,$window,$rootScope,$state,$localStorage,CoreService,gettextCatalog,BookingsService,bookingService) {
    $scope.foods = $localStorage.foodCart.foods;
    $scope.totalPrices = $localStorage.foodCart.totalPrices;
    $scope.cachePrices = $localStorage.foodCart.cachePrices;
    $scope.totalDelivery = $localStorage.foodCart.totalDelivery;
    $scope.deliveryOption = $localStorage.foodCart.deliveryOption;
    $scope.removeFood = function (index) {
      $scope.foodCart = bookingService.removeFood(index);
      $scope.cachePrices = bookingService.totalPrices();
    };
    $scope.addFood = function (food) {
      $scope.foodCart = bookingService.addFood(food);
      $scope.cachePrices = bookingService.totalPrices();
    };
    console.log($localStorage.foodCart)
    $scope.moveBookingFood = function () {
      if($scope.foods == 0){
        CoreService.alertWarning('Cảnh báo','Bạn phải chọn sản phẩm trước khi đặt hàng.');
        $state.go('index.listRestaurants')
      } else {
        $state.go('bookingFood')
      }
    }
  });
