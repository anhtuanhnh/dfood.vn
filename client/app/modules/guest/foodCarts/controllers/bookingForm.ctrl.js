'use strict';
angular
  .module('com.module.foodCarts')
  .controller('bookingFood', function ($scope,$rootScope,$localStorage,$window,$state,CoreService,Setting) {
    $scope.opened = false;
    $scope.open = function () {
      $scope.opened = !$scope.opened;
    };
    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.bookingFoods = function (stt) {
      if($scope.booking.name.length == 0){
        CoreService.alertWarning('Cảnh báo','Chúng tôi chưa tìm thấy thông tin tên người đặt hàng.')
      } else if($scope.booking.phone.length == 0 ){
        CoreService.alertWarning('Cảnh báo','Chúng tôi chưa tìm thấy thông tin số điện thoại người đặt hàng.')
      } else if($scope.booking.email.length == 0 ){
        CoreService.alertWarning('Cảnh báo','Chúng tôi chưa tìm thấy thông tin email người đặt hàng.')
      } else if($scope.booking.addressCustomer.length == 0 ){
        CoreService.alertWarning('Cảnh báo','Chúng tôi chưa tìm thấy thông tin địa chỉ người đặt hàng.')
      } else if(!$scope.booking.dateDelivery ||  !$scope.booking.timeDelivery){
        CoreService.alertWarning('Cảnh báo','Chúng tôi chưa tìm thấy thông tin thời gian người đặt hàng.')
      } else {
        $localStorage.foodCart = $scope.booking;
        $state.go('payment');
      }
    };

    $scope.foodCart = $localStorage.foodCart.foods;
    $scope.booking = $localStorage.foodCart;
    $scope.booking.cachePrices = $localStorage.foodCart.cachePrices || 0;
    $scope.booking.name = $localStorage.foodCart.name;
    $scope.booking.phone = $localStorage.foodCart.phone;
    $scope.booking.email = $localStorage.foodCart.email;
    $scope.booking.totalPrices = $localStorage.foodCart.cachePrices;
    $scope.booking.totalService = 0;
    $scope.booking.addressCustomer = '';
    $scope.booking.dateDelivery = new Date();
    $scope.booking.totalDelivery = 0 ;

    $scope.booking.deliveryOption = 'van chuyen';
    $scope.changeDeliveryOption = function (name) {
      $scope.booking.deliveryOption = name;
      if(name == 'van chuyen'){
        $scope.booking.totalPrices = $scope.booking.cachePrices + $scope.booking.totalDelivery
      } else {
        $scope.booking.totalPrices = $scope.booking.cachePrices
      }
    };
    var priceDelivery = 3500;
    Setting.findOne({
      filter: {
        where: {
          key: "priceDelivery"
        }
      }
    }, function (res) {
      console.log(res);
      priceDelivery = res.value
    });

    function initMap() {
      if(document.getElementById('pac-input1')){
        var input = document.getElementById('pac-input1');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();
          $scope.booking.addressCustomer = place.formatted_address;
          if($localStorage.foodCart.restaurant.isDelivery == false){
            var glatlng1 = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            var glatlng2 = new google.maps.LatLng($localStorage.foodCart.restaurant.lat, $localStorage.foodCart.restaurant.lng);
            var miledistance = google.maps.geometry.spherical.computeDistanceBetween(glatlng1,glatlng2);
            $scope.booking.totalDelivery = parseInt(miledistance/1000 * priceDelivery);
            $scope.booking.totalPrices = $scope.booking.cachePrices + $scope.booking.totalDelivery;
            $scope.$apply();
          }
        });
      }
    }
    initMap();
  })
