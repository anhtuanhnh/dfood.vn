'use strict';
angular
  .module('com.module.foodCarts')
  .controller('paymentCtrl',function ($scope,$window,$rootScope,$state,$localStorage,CoreService,gettextCatalog,BookingsService) {

    $scope.booking = $localStorage.foodCart;
    $scope.booking.categoryId = $localStorage.foodCart.restaurant.id;
    $scope.booking.created = new Date();
    $scope.booking.zone  = $localStorage.zone;
    $scope.booking.status = 'Chưa giao hàng';
    $scope.booking.userId = $rootScope.currentUser.id;

    $scope.goBack = function () {
      $state.go('bookingFood');
    };

    $scope.paymentFoods = function (stt) {
        BookingsService.upsertBooking($scope.booking).then(function () {
          $localStorage.foodCart.foods = [];
          $state.go('confirm');
        });
    }
  });
