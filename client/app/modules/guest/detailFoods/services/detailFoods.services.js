'use strict';

angular
  .module('com.module.detailFoods')
  .service('bookingService', function ($localStorage,CoreService,gettextCatalog) {
    this.addFood = function (food,address,lat,lng,categoryId,isDelivery) {
      if($localStorage.foodCart.foods.length == 0 || !$localStorage.foodCart ){
        food.count = 1;
        $localStorage.foodCart.restaurant = {};
        $localStorage.foodCart.restaurant.lat = lat;
        $localStorage.foodCart.restaurant.lng = lng;
        $localStorage.foodCart.restaurant.id = categoryId;
        $localStorage.foodCart.restaurant.isDelivery = isDelivery || false;
        $localStorage.foodCart.restaurant.address = address;
        $localStorage.foodCart.foods.push(food);
        CoreService.toastSuccess('Dfood thông báo', "Thêm sản phẩm vào giỏ hàng thành công");
        return $localStorage.foodCart.foods
      } else {
        for(var i = 0;i<$localStorage.foodCart.foods.length;i++){
          if($localStorage.foodCart.foods[i].name == food.name){
            $localStorage.foodCart.foods[i].count += 1;
            CoreService.toastSuccess('Dfood thông báo', "Thêm sản phẩm vào giỏ hàng thành công");
            return $localStorage.foodCart.foods;
          } else if(i == $localStorage.foodCart.foods.length -1){
            food.count = 1;
            $localStorage.foodCart.foods.push(food);
            CoreService.toastSuccess('Dfood thông báo', "Thêm sản phẩm vào giỏ hàng thành công");
            return $localStorage.foodCart.foods;
          }
        }
      }
    };
    this.removeFood = function (index) {
      if( $localStorage.foodCart.foods[index].count == 1){
        $localStorage.foodCart.foods.splice(index,1);
        return $localStorage.foodCart.foods
      } else {
        $localStorage.foodCart.foods[index].count -= 1;
        return $localStorage.foodCart.foods
      }
      CoreService.toastSuccess(
        gettextCatalog.getString('Xóa sản phẩm khỏi giỏ hàng thành công.')
      );
    };
    this.totalPrices = function () {
      $localStorage.foodCart.cachePrices = 0;
      $localStorage.foodCart.foods.forEach(function (item) {
        if(item.price){
          $localStorage.foodCart.cachePrices += item.price*item.count;
        }
      });
      return $localStorage.foodCart.cachePrices
    }
  });
