'use strict';
angular
  .module('com.module.detailFoods')
  .controller('detail', function ($scope,$state,$localStorage,$rootScope,$stateParams,Product,Category,CoreService,gettextCatalog,bookingService) {
    if($localStorage.foodCart){
      $scope.totalPrices = $localStorage.foodCart.totalPrices;
      $scope.foodCart = $localStorage.foodCart.foods;
    } else {
      $scope.totalPrices = 0;
      $scope.foodCart = []
    }
    $localStorage.zone = $stateParams.zone;
    Category.findOne({
      filter:{
        include: 'products',
        where:{
          id: $stateParams.id
        }
      }
    }, function (result) {
      console.log(result)
      $scope.restaurant = result;
    });
    $scope.deliveryOption = 'van chuyen';
    $scope.changeDeliveryOption = function (name) {
      $scope.deliveryOption = name
    };
    $scope.removeFood = function (index) {
      $scope.foodCart = bookingService.removeFood(index);
      $scope.totalPrices = bookingService.totalPrices();
    };
    $scope.addFood = function (food,restaurant) {
      $scope.foodCart = bookingService.addFood(food,restaurant.address,restaurant.lat,restaurant.lng,restaurant.id,restaurant.isDelivery);
      $scope.totalPrices = bookingService.totalPrices();
    };
    $scope.bookingNow = function () {
      if($scope.foodCart == 0 || $scope.totalPrices == []){
        CoreService.alertWarning('Cảnh báo','Bạn phải chọn sản phẩm trước khi đặt hàng.')
      } else if($rootScope.currentUser && $rootScope.currentUser.id){
        $state.go('foodCarts');
      } else {
        CoreService.confirm(
          gettextCatalog.getString('Sử dụng tài khoản Dfood để đặt hàng.'),
          gettextCatalog.getString('Nhấn Oke để đăng ký/ đăng nhập'),
          function () {
            $state.go('index.login', {url: '/carts'});
          },
          function () {
          }
        );
      }
    };
  })

;

