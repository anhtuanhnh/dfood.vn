'use strict';
angular
  .module('com.module.detailFoods')
  .config(function($stateProvider,$locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
      .state('detail', {
        url: '/detail/:id',
        views: {
          '': {
            templateUrl: 'modules/guest/detailFoods/views/detail.html',
            controller: 'detail'
          },
          'detail_header@detail': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'
          },
          'detail_info@detail': {
            templateUrl: 'modules/guest/detailFoods/views/detail_info.html'
          },
          'detail_main@detail': {
            templateUrl: 'modules/guest/detailFoods/views/detail_main.html'
          },
          'detail_footer@detail': {
            templateUrl: 'modules/guest/index/views/index_footer.html',
            controller: ''

          }
        },
        params: {
          'zone': ''
        }
      })

  });
