'use strict';
angular
  .module('com.module.submitForms')
  .config(function($stateProvider,$locationProvider) {
    $locationProvider.html5Mode(true);
    $stateProvider
      .state('submitDriver', {
        url: '/submitDriver',
        views: {
          '': {
            templateUrl: 'modules/guest/submitForms/views/submitDriver/submitDriver.html',
            controller: 'submitForms'
          },
          'submitDriver_header@submitDriver': {
            templateUrl: 'modules/guest/index/views/index_header.html',
            controller: 'index'
          },
          'submitDriver_info@submitDriver': {
            templateUrl: 'modules/guest/submitForms/views/submitDriver/submitDriver_info.html'
          },
          'submitDriver_main@submitDriver': {
            templateUrl: 'modules/guest/submitForms/views/submitDriver/submitDriver_main.html'
          },
          'submitDriver_form@submitDriver': {
            templateUrl: 'modules/guest/submitForms/views/submitDriver/submitDriver_form.html',
            controller: 'submitForms'
          },
          'submitDriver_footer@submitDriver': {
            templateUrl: 'modules/guest/index/views/index_footer.html',
            controller: ''

          }
        }
      })
    .state('submitRestaurant', {
      url: '/submitRestaurant',
      views: {
        '': {
          templateUrl: 'modules/guest/submitForms/views/submitRestaurant/submitRestaurant.html',
          controller: 'submitForms'
        },
        'submitRestaurant_header@submitRestaurant': {
          templateUrl: 'modules/guest/index/views/index_header.html',
          controller: 'index'
        },
        'submitRestaurant_info@submitRestaurant': {
          templateUrl: 'modules/guest/submitForms/views//submitRestaurant/submitRestaurant_info.html'
        },
        'submitRestaurant_main@submitRestaurant': {
          templateUrl: 'modules/guest/submitForms/views//submitRestaurant/submitRestaurant_main.html'
        },
        'submitRestaurant_plans@submitRestaurant': {
          templateUrl: 'modules/guest/submitForms/views//submitRestaurant/submitRestaurant_plans.html'
        },
        'submitRestaurant_form@submitRestaurant': {
          templateUrl: 'modules/guest/submitForms/views//submitRestaurant/submitRestaurant_form.html'
        },
        'submitRestaurant_footer@submitRestaurant': {
          templateUrl: 'modules/guest/index/views/index_footer.html',
          controller: ''
        }
      }
    })
  });
