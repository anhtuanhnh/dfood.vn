'use strict';
angular
  .module('com.module.submitForms')
  .controller('submitForms', function ($scope,$state,Driver,CoreService, Restaurant) {
    $scope.form = {};
    $scope.submitFormDrive = function () {
      $scope.form.created = new Date();
      Driver.upsert($scope.form,function (res) {
        console.log(res)
        if(res.id){
          CoreService.toastSuccess('Thông tin của bạn đã được gửi đi', "Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất");
          $state.go('index.home')
        }
      });
    }
    $scope.formRestaurant = {};
    $scope.submitFormRestaurant = function () {
      $scope.formRestaurant.created = new Date();
      Restaurant.upsert($scope.formRestaurant,function (res) {
        console.log(res)
        if(res.id){
          CoreService.toastSuccess('Thông tin của bạn đã được gửi đi', "Chúng tôi sẽ liên hệ lại với bạn trong thời gian sớm nhất");
          $state.go('index.home')
        }
      });
    }
  });

