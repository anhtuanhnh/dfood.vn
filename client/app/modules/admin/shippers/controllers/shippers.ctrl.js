(function () {
  'use strict';

  angular
    .module('com.module.shippers')
    .controller('ManageShipperCtrl', function ($scope,User, uiGridConstants) {
      $scope.shippers = [];


      $scope.dataset = [];

      var loadData = function () {
        User.find({
          filter:{
            where:{
              role: 'shipper'
            }
          }
        }, function (res) {
          console.log(res);
          for (var i = 0; i < res.length; i++) {
            var newRow = {
              'id': res[i].id,
              'name': res[i].username,
              'zone': res[i].zone,
              'shift': res[i].shift,
              'status': res[i].status,
              'phone': res[i].phone,
              'bookingDone': res[i].bookingDone,
              'bookingDoing': res[i].bookingDoing,
              'totalPriceBooking': res[i].totalPriceBooking,
            };
            $scope.dataset.push(newRow);
          }
        })

      };

      loadData();

      $scope.gridOptions = {
        showGridFooter: true,
        showColumnFooter: true,
        enableFiltering: true,
        columnDefs: [
          {
            field: 'id',
            width: '10%',
            displayName: 'Mã NV'
          },
          {
            field: 'name',
            width: '10%',
            displayName: 'Tên NV'
          },
          {
            field: 'zone',
            width: '10%',
            displayName: 'Khu vực'
          },
          {
            field: 'shift',
            aggregationHideLabel: true,
            width: '10%',
            displayName: 'Ca trực'
          },
          {
            field: 'status',
            aggregationHideLabel: true,
            width: '10%',
            displayName: 'Tình trạng'
          },
          {
            name: 'phone',
            field: 'phone',
            width: '10%',
            displayName: 'SDT'
          },
          {
            name: 'bookingDone',
            field: 'bookingDone',
            width: '13%',
            displayName: 'Đơn hàng đã giao'
          },
          {
            name: 'bookingDoing',
            field: 'bookingDoing',
            width: '14%',
            displayName: 'Đơn hàng đang giao'
          },
          {
            name: 'totalPriceBooking',
            field: 'totalPriceBooking',
            width: '13%',
            displayName: 'Tổng tiền nhận'
          }
        ],
        data: $scope.dataset,
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;
        }
      };

    });

})();
