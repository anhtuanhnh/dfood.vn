(function () {
  'use strict';
  angular
    .module('com.module.shippers')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.shippers', {
          abstract: true,
          url: '/shippers',
          templateUrl: 'modules/admin/shippers/views/main.html'
        })
        .state('app.shippers.list', {
          url: '',
          templateUrl: 'modules/admin/shippers/views/list.html',
          controller: 'ManageShipperCtrl'
        })
        .state('app.shippers.add', {
          url: '/add',
          templateUrl: 'modules/admin/shippers/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, ShippersService) {
            this.shipper = {
              status: true
            };
            this.formFields = ShippersService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              ShippersService.upsertShipper(this.shipper).then(function () {
                $state.go('^.list');
              });
            };
          }
        })
        .state('app.shippers.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/admin/shippers/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, ShippersService, shipper) {
            this.shipper = shipper;
            this.formFields = ShippersService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              ShippersService.upsertShipper(this.shipper).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            shipper: function ($stateParams, ShippersService) {
              return ShippersService.getShipper($stateParams.id);
            }
          }
        })
        .state('app.shippers.view', {
          url: '/:id',
          templateUrl: 'modules/admin/shippers/views/view.html',
          controllerAs: 'ctrl',
          controller: function (shipper) {
            this.shipper = shipper;
            console.log(shipper);
          },
          resolve: {
            shipper: function ($stateParams, ShippersService) {
              return ShippersService.getShipper($stateParams.id);
            }
          }
        })
        .state('app.shippers.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, ShippersService, shipper) {
            ShippersService.deleteShipper(shipper.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            shipper: function ($stateParams, ShippersService) {
              return ShippersService.getShipper($stateParams.id);
            }
          }
        });
    });

})();
