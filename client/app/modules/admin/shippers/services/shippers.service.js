(function () {
  'use strict';
  angular
    .module('com.module.shippers')
    .service('ShippersService', function (CoreService, User, gettextCatalog) {
      this.getShippers = function () {
        return User.find({
          filter: {
            where:{
              role: 'shipper'
            },
            order: 'created DESC'
          }
        }).$promise;
      };

      this.getShipper = function (id) {
        return User.findById({
          id: id
        }).$promise;
      };

      this.upsertShipper = function (shipper) {
        return User.upsert(shipper).$promise
          .then(function () {
            CoreService.toastSuccess(
              gettextCatalog.getString('Shipper saved'),
              gettextCatalog.getString('Your shipper is safe with us!')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Error saving shipper '),
              gettextCatalog.getString('This shipper could no be saved: ') + err
            );
          }
        );
      };

      this.deleteShipper = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            User.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Shipper deleted'),
                gettextCatalog.getString('Your shipper is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting Shipper'),
                gettextCatalog.getString('Your Shipper is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };

      this.getFormFields = function () {
        var options = [{
          name: 'Active',
          value: true
        },{
          name: 'Inactive',
          value: false
        }];
        return [
          {
            key: 'email',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Địa chỉ email'),
              required: true
            }
          },
          {
            key: 'firstName',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Họ nhân viên'),
              required: true
            }
          },
          {
            key: 'lastName',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên nhân viên'),
              required: true
            }
          },
          {
            key: 'zone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Khu vực làm việc'),
              required: true
            }
          },
          {
            key: 'shift',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Ca làm việc'),
              required: true
            }
          },
          {
            key: 'status',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Tình trạng'),
              required: true,
              options: options
            }
          },
          {
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại'),
              required: true
            }
          }
        ];
      };
    });

})();
