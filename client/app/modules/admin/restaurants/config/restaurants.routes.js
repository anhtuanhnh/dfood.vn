(function () {
  'use strict';
  angular
    .module('com.module.restaurants')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.restaurants', {
          abstract: true,
          url: '/restaurants',
          templateUrl: 'modules/admin/restaurants/views/main.html'
        })
        .state('app.restaurants.list', {
          url: '',
          templateUrl: 'modules/admin/restaurants/views/list.html',
          controller: 'RestaurantCtrl'
        })
        .state('app.restaurants.add', {
          url: '/add',
          templateUrl: 'modules/admin/restaurants/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state,$stateParams, RestaurantsService,restaurant,CoreService,FileUploader) {
            this.restaurant = restaurant;

            function initMap() {
              var map = new google.maps.Map(document.getElementById('gmaps'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
              var card = document.getElementById('pac-card2');
              var input = document.getElementById('pac-input2');
              var types = document.getElementById('type-selector');
              var strictBounds = document.getElementById('strict-bounds-selector');


              var autocomplete = new google.maps.places.Autocomplete(input);

              // Bind the map's bounds (viewport) property to the autocomplete object,
              // so that the autocomplete requests use the current map bounds for the
              // bounds option in the request.
              autocomplete.bindTo('bounds', map);

              var infowindow = new google.maps.InfoWindow();
              var infowindowContent = document.getElementById('infowindow-content');
              infowindow.setContent(infowindowContent);
              var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
              });

              autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                restaurant.address = place.formatted_address;
                restaurant.lat = place.geometry.location.lat();
                restaurant.lng = place.geometry.location.lng()
                if (!place.geometry) {
                  // User entered the name of a Place that was not suggested and
                  // pressed the Enter key, or the Place Details request failed.
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

              });

            }

            initMap();
            this.formFields = RestaurantsService.getFormFields();
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.restaurant.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              RestaurantsService.upsertRestaurant(this.restaurant).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            restaurant: function () {
              return {};
            }
          }
        })
        .state('app.restaurants.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/admin/restaurants/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, RestaurantsService, restaurant,FileUploader,CoreService) {
            this.restaurant = restaurant;

            function initMap() {
              var map = new google.maps.Map(document.getElementById('gmaps'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13
              });
              var card = document.getElementById('pac-card2');
              var input = document.getElementById('pac-input2');
              var types = document.getElementById('type-selector');
              var strictBounds = document.getElementById('strict-bounds-selector');


              var autocomplete = new google.maps.places.Autocomplete(input);

              // Bind the map's bounds (viewport) property to the autocomplete object,
              // so that the autocomplete requests use the current map bounds for the
              // bounds option in the request.
              autocomplete.bindTo('bounds', map);

              var infowindow = new google.maps.InfoWindow();
              var infowindowContent = document.getElementById('infowindow-content');
              infowindow.setContent(infowindowContent);
              var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
              });

              autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                restaurant.address = place.formatted_address;
                restaurant.lat = place.geometry.location.lat();
                restaurant.lng = place.geometry.location.lng()
                if (!place.geometry) {
                  // User entered the name of a Place that was not suggested and
                  // pressed the Enter key, or the Place Details request failed.
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

              });

            }

            initMap();
            this.formFields = RestaurantsService.getFormFields();
            this.formOptions = {};
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.restaurant.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              RestaurantsService.upsertRestaurant(this.restaurant).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            restaurant: function ($stateParams, RestaurantsService) {
              return RestaurantsService.getRestaurant($stateParams.id);
            }
          }
        })
        .state('app.restaurants.view', {
          url: '/:id',
          templateUrl: 'modules/admin/restaurants/views/view.html',
          controllerAs: 'ctrl',
          controller: function (restaurant) {
            this.restaurant = restaurant;
          },
          resolve: {
            restaurant: function ($stateParams, RestaurantsService) {
              return RestaurantsService.getRestaurant($stateParams.id);
            }
          }
        })
        .state('app.restaurants.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, RestaurantsService, restaurant) {
            RestaurantsService.deleteRestaurant(restaurant.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            restaurant: function ($stateParams, RestaurantsService) {
              return RestaurantsService.getRestaurant($stateParams.id);
            }
          }
        });
    }
  );

})();
