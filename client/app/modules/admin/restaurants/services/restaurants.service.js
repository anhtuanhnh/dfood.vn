(function () {
  'use strict';
  angular
    .module('com.module.restaurants')
    .service('RestaurantsService', function (CoreService, Category, gettextCatalog,User) {
      this.getRestaurants = function (zone) {
        return Category.find({
          filter: {
            where:{
              zone: zone
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getRestaurant = function (id) {
        return Category.findById({
          id: id
        }).$promise;
      };

      this.upsertRestaurant = function (restaurant) {
        return Category.upsert(restaurant).$promise
          .then(function (result) {
            console.log(result);
            CoreService.toastSuccess(
              gettextCatalog.getString('Bạn đã đặt món thàn công'),
              gettextCatalog.getString('Chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Đặt hàng bị lỗi'),
              gettextCatalog.getString('This restaurant could no be saved: ') + err
            );
          }
        );
      };

      this.deleteRestaurant = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            Category.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Restaurant deleted'),
                gettextCatalog.getString('Your restaurant is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting restaurant'),
                gettextCatalog.getString('Your restaurant is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };

      var optionsUser = [];
      User.find({
        filter:{
          fields: ['email','id'],
          where:{
            role: 'consumer'
          }
        }
      },function (res) {
        res.forEach(function (item) {
          optionsUser.push({name: item.email,value: item.id})
        })
      });
      var option = [{
        name: "Huế",
        value: "Hue"
      },
        {
          name: 'Đà Nẵng',
          value: "Danang"
        },
        {
          name:'Quảng Trị',
          value: "Quangtri"
        }];
      var optionTypeRestaurant = [{
        name: "Nhà hàng",
        value: "Nhà hàng"
      },
        {
          name: 'Quán',
          value: "Quán"
        },
        {
          name:'Cửa hàng',
          value: "Cửa hàng"
        },
        {
          name:'Siêu thị',
          value: "Siêu thị"
        }];
      var optionIsDelivery = [{
        name: "Có",
        value: true },{
        name: "Không",
        value: false
      }
      ];
      this.getFormFields = function () {
        return [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên nhà hàng'),
              required: true
            }
          },
          {
            key: 'ownerId',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Quản lý'),
              options: optionsUser,
              required: true
            }
          },
          {
            key: 'zone',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Khu vực'),
              options: option,
              required: true
            }
          },

          {
            key: 'type',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Loại nhà hàng'),
              required: true,
              options: optionTypeRestaurant
            }
          },
          {
            key: 'isDelivery',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Dịch vụ vận chuyển riêng'),
              required: true,
              options: optionIsDelivery
            }
          },
          {
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại liên lạc'),
              required: true
            }
          },
          {
            key: 'close',
            type: 'timepicker',
            templateOptions: {
              label: gettextCatalog.getString('Giờ đóng cửa'),
              required: true,
              id: 'datetimepicker1'
            }
          },
          {
            key: 'open',
            type: 'timepicker',
            templateOptions: {
              label: gettextCatalog.getString('Giờ mở cửa'),
              required: true,
              id: 'datetimepicker2'
            }
          },
          {
            key: 'common',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Mức độ phổ biến')
            }
          },
          {
            key: 'commission',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Phần trăm hoa hồng')
            }
          }
        ];
      };

    });

})();
