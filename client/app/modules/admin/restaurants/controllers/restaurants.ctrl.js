(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.restaurants')
    .controller('RestaurantCtrl', function ($scope, $rootScope, Category,User) {
      var user = $rootScope.currentUser;
      $scope.restaurants = {};
      Category.find({
        filter:{
          include: {
            relation: 'user',
            scope:{
              fields: ['email']
            }
          }
        }
      }, function (res) {
        console.log(res);
        $scope.restaurants = res;
      })

    });

})();
