(function () {
  'use strict';
  angular
    .module('com.module.formDriver')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.formDriver', {
          abstract: true,
          url: '/formDriver',
          templateUrl: 'modules/admin/formDriver/views/main.html'
        })
        .state('app.formDriver.list', {
          url: '',
          templateUrl: 'modules/admin/formDriver/views/list.html',
          controller: 'FormDriverCtrl'
        })
    }
  );

})();
