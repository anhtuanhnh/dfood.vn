(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.formDriver')
    .controller('FormDriverCtrl', function ($scope, $rootScope, Category,Driver) {

      Driver.find({
        filter:{
          order: 'id DESC'
        }
      }, function (res) {
        $scope.drivers = res
      })
    });

})();
