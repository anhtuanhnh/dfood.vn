(function () {
  'use strict';
  angular
    .module('com.module.foods')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.foods', {
          abstract: true,
          url: '/foods',
          templateUrl: 'modules/admin/foods/views/main.html'
        })
        .state('app.foods.list', {
          url: '',
          templateUrl: 'modules/admin/foods/views/list.html',
          controllerAs: 'ctrl',
          controller: function (foods) {
            console.log(foods);
            this.foods = foods;
          },
          resolve: {
            foods: function (FoodsService) {
                return FoodsService.getFoods();
              }
          }
        })
        .state('app.foods.add', {
          url: '/add',
          templateUrl: 'modules/admin/foods/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, FoodsService,Category, restaurants,CoreService,FileUploader) {
            this.restaurants = restaurants;
            this.food = {};
            this.formFields = FoodsService.getFormFields(restaurants);
            this.formOptions = {};
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.food.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              FoodsService.upsertFood(this.food).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            restaurants: function (RestaurantsService) {
              return RestaurantsService.getRestaurants();
            }
          }
        })
        .state('app.foods.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/admin/foods/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, FoodsService,Category, restaurants, food,CoreService,FileUploader) {
            this.restaurants = restaurants;
            this.food = food;
            this.formFields = FoodsService.getFormFields(restaurants);
            this.formOptions = {};
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.food.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              FoodsService.upsertFood(this.food).then(function () {
                $state.go('^.list');
              });
            }
          },
          resolve: {
            restaurants: function (RestaurantsService) {
              return RestaurantsService.getRestaurants();
            },
            food: function ($stateParams, FoodsService) {
              return FoodsService.getFood($stateParams.id);
            }
          }
        })
        .state('app.foods.view', {
          url: '/:id',
          templateUrl: 'modules/admin/foods/views/view.html',
          controllerAs: 'ctrl',
          controller: function (food) {
            this.food = food;
            console.log(food);
          },
          resolve: {
            food: function ($stateParams, FoodsService) {
              return FoodsService.getFood($stateParams.id);
            }
          }
        })
        .state('app.foods.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, FoodsService, food) {
            FoodsService.deleteFood(food.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            food: function ($stateParams, FoodsService) {
              return FoodsService.getFood($stateParams.id);
            }
          }
        });
    });

})();
