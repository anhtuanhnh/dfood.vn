(function () {
  'use strict';
  angular
    .module('com.module.foods')
    .service('FoodsService', function (CoreService, Product, gettextCatalog) {

      this.getFoods = function () {
        return Product.find({
          filter: {
            include: {
              relation: 'category', // include the owner object
              scope: { // further filter the owner object
                fields: ['name'], // only show two fields
              }
            },
            order: 'created DESC'
          }
        }, function(res){
          console.log(res);
        }).$promise;
      };

      this.getFood = function (id) {
        return Product.findById({
          id: id
        }).$promise;
      };

      this.upsertFood = function (food) {
        return Product.upsert(food).$promise
          .then(function (res) {
            console.log(res);
            CoreService.toastSuccess(
              gettextCatalog.getString('Food saved'),
              gettextCatalog.getString('Your food is safe with us!')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Error saving food '),
              gettextCatalog.getString('This food could no be saved: ') + err
            );
          }
        );
      };

      this.deleteFood = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            Product.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Food deleted'),
                gettextCatalog.getString('Your food is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting food'),
                gettextCatalog.getString('Your food is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };

      this.getFormFields = function (restaurants) {
        var restaurantOptions = restaurants.map(function (item) {
          return {
            name: item.name,
            value: item.id
          }
        });
        var foodTypes = [{
          name: 'Đồ ngọt',
          value: 'Đồ ngọt'
        },{
          name: 'Đồ chay',
          value: 'Đồ chay'
        },
          {
            name: 'Đồ mặn',
            value: 'Đồ mặn'
          },
          {
            name: 'Nguyên liệu',
            value: 'Nguyên liệu'
          }];
        return [
          {
            key: 'categoryId',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Tên nhà hàng'),
              options: restaurantOptions,
              required: true
            }
          },{
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên món ăn'),
              required: true
            }
          },
          {
            key: 'introduce',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Giới thiệu')
            }
          },{
            key: 'type',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Loại'),
              options: foodTypes
            }
          },
          {
            key: 'price',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Giá')
            }
          },
          {
            key: 'unit',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Đơn vị '),
              placeholder: 'Kg/g/mlg/cai/combo...'
            }
          },
          {
            key: 'timeProcess',
            type: 'timeAgo',
            templateOptions: {
              label: gettextCatalog.getString('Thời gian chế biến')
            }
          },
          {
            key: 'promotion',
            type: 'input',
            templateOptions: {
              valueDefault: 100,
              label: gettextCatalog.getString('Chương trình khuyến mãi')
            }
          }
        ];
      };
    });

})();
