(function () {
  'use strict';
  angular
    .module('com.module.formRestaurant')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.formRestaurant', {
          abstract: true,
          url: '/formRestaurant',
          templateUrl: 'modules/admin/formRestaurant/views/main.html'
        })
        .state('app.formRestaurant.list', {
          url: '',
          templateUrl: 'modules/admin/formRestaurant/views/list.html',
          controller: 'FormRestaurantCtrl'
        })
    }
  );

})();
