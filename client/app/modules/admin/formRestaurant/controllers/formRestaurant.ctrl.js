(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.formRestaurant')
    .controller('FormRestaurantCtrl', function ($scope, $rootScope, Category,Restaurant) {

      Restaurant.find({
        filter:{
          order: 'id DESC'
        }
      }, function (res) {
        $scope.restaurants = res
      })
    });

})();
