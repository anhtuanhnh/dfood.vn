(function () {
  'use strict';
  angular
    .module('com.module.bookings')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.bookings', {
          abstract: true,
          url: '/bookings',
          templateUrl: 'modules/admin/bookings/views/main.html'
        })
        .state('app.bookings.list', {
          url: '',
          templateUrl: 'modules/admin/bookings/views/list.html',
          controller: 'BookingCtrl'
        })
        .state('app.bookings.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/admin/bookings/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, BookingsService, booking) {
            console.log(booking);
            this.booking = booking;
            this.formFields = BookingsService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              BookingsService.upsertBooking(this.booking).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            booking: function ($stateParams, BookingsService) {
              return BookingsService.getBooking($stateParams.id);
            }
          }
        })
        .state('app.bookings.view', {
          url: '/:id',
          templateUrl: 'modules/admin/bookings/views/view.html',
          controllerAs: 'ctrl',
          controller: function (booking,BookingsService,$localStorage) {
            this.booking = booking;
            this.user = $localStorage.user;
            console.log(booking,this.user);
            this.editFoods = function (index,operator,value) {
              if(operator =='plus'){
                this.booking.foods[index].count += 1;
                totalPriceFuc();
              } else if (operator == 'subtract'){
                if(value == 0){
                  console.log('Khong con gi de tru')
                } else {
                  this.booking.foods[index].count -= 1;
                  totalPriceFuc();
                }
              }
            }
             var totalPriceFuc = function () {
               this.booking.totalPrices = 0;
               this.booking.foods.forEach(function (item) {
                 this.booking.totalPrices += item.price * item.count
              }.bind(this));
               BookingsService.upsertBooking(this.booking)
            }.bind(this)
          },
          resolve: {
            booking: function ($stateParams, BookingsService) {
              return BookingsService.getBooking($stateParams.id);
            }
          }
        })
        .state('app.bookings.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, BookingsService, booking) {
            BookingsService.deleteBooking(booking.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            booking: function ($stateParams, BookingsService) {
              return BookingsService.getBooking($stateParams.id);
            }
          }
        });
    }
  );

})();
