(function () {
  'use strict';
  angular
    .module('com.module.bookings')
    .service('BookingsService', function (CoreService, Booking, gettextCatalog,User) {
      this.getBookings = function (zone) {
        return Booking.find({
          filter: {
            where:{
              zone: zone
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBookingById = function (id) {
        return Booking.find({
          filter: {
            where:{
              userId: id
            },
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBookingByStatus = function (status) {
        return Booking.find({
          filter: {
            order: 'id DESC'
          }
        }).$promise;
      };

      this.getBooking = function (id) {
        return Booking.findById({
          id: id
        }).$promise;
      };

      this.upsertBooking = function (booking) {
        return Booking.upsert(booking).$promise
          .then(function (result) {
            console.log(result);
            CoreService.toastSuccess(
              gettextCatalog.getString('Bạn đã đặt món thàn công'),
              gettextCatalog.getString('Chúng tôi sẽ liên hệ lại bạn trong thời gian sớm nhất')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Đặt hàng bị lỗi'),
              gettextCatalog.getString('This booking could no be saved: ') + err
            );
          }
        );
      };

      this.deleteBooking = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            Booking.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Booking deleted'),
                gettextCatalog.getString('Your booking is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting booking'),
                gettextCatalog.getString('Your booking is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };


      this.getFormFields = function () {
        var listShipper = [];
        var listStatus = [
          {
            value: 'Chưa giao hàng',
            name: 'Chưa giao hàng'
          },
          {
            value: 'Đang giao hàng',
            name: 'Đang giao hàng'
          },
          {
            value: 'Hoàn thành',
            name: 'Hoàn thành'
          }
        ]
        User.find({
          filter:{
            where:{
              role: 'shipper'
            }
          }
        },function (users) {
          users.forEach(function (item) {
            listShipper.push({
              name: item.username,
              value: item.id
            })
          })
        });
        return [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên đơn hàng'),
              required: true
            }
          },

          {
            key: 'email',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Email')
            }
          },
          {
            key: 'addressCustomer',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Địa chỉ giao hàng')
            }
          },
          {
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại')
            }
          },
          {
            key: 'note',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Lưu ý')
            }
          },
          {
            key: 'dateDelivery',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Ngày vận chuyển')
            }
          },
          {
            key: 'timeDelivery',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Giờ vận chuyển')
            }
          },
          {
            key: 'shipperId',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Chọn nhân viên giao hàng'),
              options: listShipper
            }
          },
          {
            key: 'totalPrices',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tổng tiền')
            }
          },
          {
            key: 'status',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Thay đổi tình trạng'),
              options: listStatus
            }
          }
        ];
      };

    });

})();
