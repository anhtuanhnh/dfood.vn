(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.bookings')
    .controller('BookingCtrl', function ($scope, $rootScope, Booking,createChangeStream,LiveSet,CoreService,Category) {
      var user = $rootScope.currentUser;
      $scope.bookings = [];

      var src = new EventSource('/api/bookings/change-stream');
      var changes = createChangeStream(src);
      var set;

      Booking.find().$promise.then(function(results) {
        // $scope.bookings.forEach(function (item) {
        //   if(item.status == 'Hủy đơn hàng'){
        //     Category.findById({
        //       id: item.categoryId
        //     }, function (restaurant) {
        //       CoreService.alertWarning('Cảnh báo đơn hàng'+ item.id + ' bị hủy','Đơn hàng của nhà hàng'+ restaurant.name
        //       )
        //     })
        //
        //   }
        // })
        set = new LiveSet(results, changes);
        console.log(set);
        $scope.bookings = set.toLiveArray();

      });
    });

})();
