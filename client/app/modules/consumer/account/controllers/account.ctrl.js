(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.account')
    .controller('AccountCtrl', function ($scope, $rootScope,UserService,$state) {
      if($rootScope.currentUser){
        $scope.user = $rootScope.currentUser;
        $scope.user.dateRegister = '10/03/2017'
      }
      $scope.editStatus = true;
      $scope.editStatusFirst = true;
      $scope.updateAccount = function (nameClass) {
        UserService.upsert($scope.user).then(function () {
          $state.go('^.list');

          if(nameClass == 'box-1'){
            $scope.editStatusFirst = true;
          } else {
            $scope.editStatus = true;
          }
          $('.'+nameClass).removeClass('acctiveEdit')
        });
      };
      $scope.editStyle = function (nameClass) {
        $('.'+nameClass).toggleClass('acctiveEdit');
        if(nameClass == 'box-1'){
          $scope.editStatusFirst = !$scope.editStatusFirst;
        } else {
          $scope.editStatus = !$scope.editStatus;
        }
      }
    });

})();
