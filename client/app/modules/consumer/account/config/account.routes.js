(function () {
  'use strict';
  angular
    .module('com.module.account')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.account', {
          abstract: true,
          url: '/account',
          templateUrl: 'modules/consumer/account/views/main.html'
        })
        .state('app.account.list', {
          url: '',
          templateUrl: 'modules/consumer/account/views/list.html',
          controller: 'AccountCtrl'
        })
    }
  );

})();
