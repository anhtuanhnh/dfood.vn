(function () {
  'use strict';
  angular
    .module('com.module.invoices')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.invoices', {
          abstract: true,
          url: '/invoices',
          templateUrl: 'modules/consumer/invoices/views/main.html'
        })
        .state('app.invoices.list', {
          url: '',
          templateUrl: 'modules/consumer/invoices/views/list.html',
          controller: 'invoiceCtrl'
        })
    }
  );

})();
