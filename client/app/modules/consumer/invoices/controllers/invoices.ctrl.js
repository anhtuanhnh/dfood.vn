(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.invoices')
    .controller('invoiceCtrl', function ($scope, Booking) {
      $scope.dateNow = new Date().toLocaleDateString();
      $scope.showDetail = function(){
        $('.first-detail').toggleClass('show-detail bounceInUp')
      };
      Booking.find({
        filter:{
          where:{

          }
        }
      })
    });

})();
