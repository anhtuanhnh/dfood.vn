(function () {
  'use strict';
  angular
    .module('com.module.consumerBookings')
    .service('ConsumerBookingsService', function (CoreService, Booking, gettextCatalog,User) {
      this.getBooking = function (id) {
        return Booking.findById({
          id: id
        }).$promise;
      };
    });

})();
