(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.consumerBookings')
    .controller('ConsumerBookingCtrl', function ($scope, $rootScope,Category, Booking,CoreService,gettextCatalog) {
      var user = $rootScope.currentUser;
      $scope.bookings = {};
      Category.findOne({
        filter:{
          where:{
            ownerId: user.id
          }
        }
      }, function (category) {
        console.log(category)
        Booking.find({
          filter:{
            where:{
              categoryId: category.id
            }
          }
        }, function (res) {
          console.log(res);
          $scope.bookings = res;
        })
      })
      $scope.cancelBooking = function (id) {
        CoreService.confirm(
          gettextCatalog.getString('Hủy đơn hàng'),
          gettextCatalog.getString('Hủy đơn hàng sẽ khiến uy tín nhà hàng giảm sút, bạn có muốn tiếp tục ?'),
          function () {
            $scope.bookings[id].status = 'Hủy đơn hàng';
            Booking.upsert($scope.bookings[id],function (res) {
              console.log(res);
              CoreService.toastSuccess('Hủy đơn hàng thành công','')
            })
          },
          function () {
            CoreService.toastWarning(
              gettextCatalog.getString('Yêu cầu hủy đơn hàng đã bị hủy'),
              gettextCatalog.getString('Đó là sự lựa chọn đúng đắn, liên hệ chúng tôi nếu bạn cần giúp đỡ')
            );
          }
        );

      }
    });

})();
