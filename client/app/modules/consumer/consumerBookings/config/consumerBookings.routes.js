(function () {
  'use strict';
  angular
    .module('com.module.consumerBookings')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.consumerBookings', {
          abstract: true,
          url: '/consumerBookings',
          templateUrl: 'modules/consumer/consumerBookings/views/main.html'
        })
        .state('app.consumerBookings.list', {
          url: '',
          templateUrl: 'modules/consumer/consumerBookings/views/list.html',
          controller: 'ConsumerBookingCtrl'
        })
        .state('app.consumerBookings.view', {
          url: '/:id',
          templateUrl: 'modules/consumer/consumerBookings/views/view.html',
          controllerAs: 'ctrl',
          controller: function (booking,ConsumerBookingsService,$localStorage) {
            this.booking = booking;
            this.user = $localStorage.user;
            console.log(booking,this.user);
            this.editFoods = function (index,operator,value) {
              if(operator =='plus'){
                this.booking.foods[index].count += 1;
                totalPriceFuc();
              } else if (operator == 'subtract'){
                if(value == 0){
                  console.log('Khong con gi de tru')
                } else {
                  this.booking.foods[index].count -= 1;
                  totalPriceFuc();
                }
              }
            }
             var totalPriceFuc = function () {
               this.booking.totalPrices = 0;
               this.booking.foods.forEach(function (item) {
                 this.booking.totalPrices += item.price * item.count
              }.bind(this));
               ConsumerBookingsService.upsertBooking(this.booking)
            }.bind(this)
          },
          resolve: {
            booking: function ($stateParams, ConsumerBookingsService) {
              return ConsumerBookingsService.getBooking($stateParams.id);
            }
          }
        })
    }
  );

})();
