(function () {
  'use strict';
  angular
    .module('com.module.categories')
    .service('CategoriesService', function (CoreService, Category, gettextCatalog,LoopBackAuth) {
      var userId = LoopBackAuth.currentUserId;
      this.getCategories = function () {
         return Category.find({
           filter: {
             order: 'created DESC',
             where:{
               ownerId: userId
             }
           }
         },function (result) {
           console.log(result)
         }).$promise;
      };

      this.getCategory = function (id) {
        return Category.findById({
          id: id
        },function (result) {
          console.log(result)
        }).$promise;
      };

      this.upsertCategory = function (category) {
        console.log(category);
        return Category.upsert(category).$promise
          .then(function () {
            CoreService.toastSuccess(
              gettextCatalog.getString('Cập nhập thông tin thành công'),
              gettextCatalog.getString('Bạn đã cập nhập thông tin nhà hàng của mình')
            );
          })
          .catch(function (err) {
            CoreService.toastSuccess(
              gettextCatalog.getString('Error saving category '),
              gettextCatalog.getString('This category could no be saved: ') + err
            );
          }
        );
      };

      this.deleteCategory = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            Category.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Category deleted'),
                gettextCatalog.getString('Your category is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting category'),
                gettextCatalog.getString('Your category is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };

      var option = [{
        name: "Huế",
        value: "Hue"
      },
        {
          name: 'Đà Nẵng',
          value: "Danang"
        },
        {
          name:'Quảng Trị',
          value: "Quangtri"
        }];
      var optionTypeRestaurant = [{
        name: "Nhà hàng",
        value: "Nhà hàng"
      },
        {
          name: 'Quán',
          value: "Quán"
        },
        {
          name:'Cửa hàng',
          value: "Cửa hàng"
        },
        {
          name:'Siêu thị',
          value: "Siêu thị"
        }];
      var optionIsDelivery = [{
        name: "Có",
        value: true },{
        name: "Không",
        value: false
      }
      ];
        this.getFormFields = function () {
          return [
            {
              key: 'name',
              type: 'input',
              templateOptions: {
                label: gettextCatalog.getString('Tên nhà hàng'),
                required: true
              }
            },
            {
              key: 'zone',
              type: 'select',
              templateOptions: {
                label: gettextCatalog.getString('Khu vực'),
                options: option,
                required: true
              }
            },
            {
              key: 'phone',
              type: 'input',
              templateOptions: {
                label: gettextCatalog.getString('Số điện thoại liên hệ'),
                required: true
              }
            },
            {
              key: 'type',
              type: 'select',
              templateOptions: {
                label: gettextCatalog.getString('Mô tả'),
                required: true,
                options: optionTypeRestaurant
              }
            },
            {
              key: 'isDelivery',
              type: 'select',
              templateOptions: {
                label: gettextCatalog.getString('Dịch vụ vận chuyển riêng'),
                required: true,
                options: optionIsDelivery
              }
            },
            {
              key: 'close',
              type: 'timepicker',
              templateOptions: {
                label: gettextCatalog.getString('Giờ đóng cửa'),
                required: true,
                id: 'datetimepicker1'
              }
            },
            {
              key: 'open',
              type: 'timepicker',
              templateOptions: {
                label: gettextCatalog.getString('Giờ mở cửa'),
                required: true,
                id: 'datetimepicker2'
              }
            }
          ];
        };

    })
})();
