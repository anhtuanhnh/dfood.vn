(function () {
  'use strict';
  angular
    .module('com.module.categories')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.categories', {
          abstract: true,
          url: '/categories',
          templateUrl: 'modules/consumer/categories/views/main.html'
        })
        .state('app.categories.view', {
          url: '/:id',
          templateUrl: 'modules/consumer/categories/views/view.html',
          controllerAs: 'ctrl',
          controller: function (category) {
            this.category = category;
          },
          resolve: {
            category: function ($stateParams,CategoriesService) {
              return CategoriesService.getCategory($stateParams.id);
            }
          }
        })
        .state('app.categories.list', {
          url: '',
          templateUrl: 'modules/consumer/categories/views/list.html',
          controllerAs: 'ctrl',
          controller: function (categories) {
            this.categories = categories;
          },
          resolve: {
            categories: [
              'CategoriesService',
              function (CategoriesService) {
                return CategoriesService.getCategories();
              }
            ]
          }
        })
        .state('app.categories.add', {
          url: '/add',
          templateUrl: 'modules/consumer/categories/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, CategoriesService,FileUploader,CoreService, category, LoopBackAuth) {
            this.category = category;

            function initMap() {
              var map = new google.maps.Map(document.getElementById('gmaps'), {
                center: {lat: -33.8688, lng: 151.2195},
                zoom: 13
              });
              var card = document.getElementById('pac-card');
              var input = document.getElementById('pac-input');
              var types = document.getElementById('type-selector');
              var strictBounds = document.getElementById('strict-bounds-selector');


              var autocomplete = new google.maps.places.Autocomplete(input);

              // Bind the map's bounds (viewport) property to the autocomplete object,
              // so that the autocomplete requests use the current map bounds for the
              // bounds option in the request.
              autocomplete.bindTo('bounds', map);

              var infowindow = new google.maps.InfoWindow();
              var infowindowContent = document.getElementById('infowindow-content');
              infowindow.setContent(infowindowContent);
              var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
              });

              autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                category.address = place.formatted_address;
                category.lat = place.geometry.location.lat();
                category.lng = place.geometry.location.lng()
                if (!place.geometry) {
                  // User entered the name of a Place that was not suggested and
                  // pressed the Enter key, or the Place Details request failed.
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                  address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                  ].join(' ');
                }


                infowindow.open(map, marker);
              });

            }

            initMap();
            var userId = LoopBackAuth.currentUserId;
            var d = new Date().toDateString();
            this.category.created = d;
            this.formFields = CategoriesService.getFormFields();
            this.formOptions = {};
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.category.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              this.category.ownerId = userId;
              console.log(this.category);
              CategoriesService.upsertCategory(this.category).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            category: function () {
              return {};
            }
          }
        })
        .state('app.categories.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/consumer/categories/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, CategoriesService, category,FileUploader,CoreService) {
            this.category = category;
            if(!category.lat){
              category.lat = 16.460776;
              category.lng = 107.584239
            }
            function initMap() {
              var map = new google.maps.Map(document.getElementById('gmaps'), {
                center: {lat: category.lat, lng: category.lng},
                zoom: 13
              });
              var card = document.getElementById('pac-card');
              var input = document.getElementById('pac-input');
              var types = document.getElementById('type-selector');
              var strictBounds = document.getElementById('strict-bounds-selector');


              var autocomplete = new google.maps.places.Autocomplete(input);

              // Bind the map's bounds (viewport) property to the autocomplete object,
              // so that the autocomplete requests use the current map bounds for the
              // bounds option in the request.
              autocomplete.bindTo('bounds', map);

              var infowindow = new google.maps.InfoWindow();
              var infowindowContent = document.getElementById('infowindow-content');
              infowindow.setContent(infowindowContent);
              var marker = new google.maps.Marker({
                map: map,
                center: {lat: category.lat, lng: category.lng},
                anchorPoint: new google.maps.Point(0, -29)
              });

              autocomplete.addListener('place_changed', function() {
                infowindow.close();

                marker.setVisible(false);
                var place = autocomplete.getPlace();
                console.log(place.geometry.location);
                category.address = place.formatted_address;
                category.lat = place.geometry.location.lat();
                category.lng = place.geometry.location.lng()
                if (!place.geometry) {
                  // User entered the name of a Place that was not suggested and
                  // pressed the Enter key, or the Place Details request failed.
                  window.alert("No details available for input: '" + place.name + "'");
                  return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                  map.fitBounds(place.geometry.viewport);
                } else {
                  map.setCenter(place.geometry.location);
                  map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                  address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                  ].join(' ');
                }


                infowindow.open(map, marker);
              });

            }

            initMap();
            this.formFields = CategoriesService.getFormFields();
            this.formOptions = {};
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.category.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              CategoriesService.upsertCategory(this.category).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            category: function ($stateParams, CategoriesService) {
              return CategoriesService.getCategory($stateParams.id);
            }
          }
        })
        .state('app.categories.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, CategoriesService, category) {
            CategoriesService.deleteCategory(category.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            category: function ($stateParams, CategoriesService) {
              return CategoriesService.getCategory($stateParams.id);
            }
          }
        })
    })
})();
