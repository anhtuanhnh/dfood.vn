(function () {
  'use strict';
  angular
    .module('com.module.products')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.products', {
          abstract: true,
          url: '/products',
          templateUrl: 'modules/consumer/products/views/main.html'
        })

        .state('app.products.list', {
          url: '/list',
          templateUrl: 'modules/consumer/products/views/list.html',
          controllerAs: 'ctrl',
          controller: function (categories) {
            this.categories = categories;
            this.indexCategory = 0;
            this.selectCategory = function (index) {
              this.indexCategory = index
            }
          },
          resolve: {
            categories: [
              'ProductsService',
              function (ProductsService) {
                return ProductsService.getProducts();
              }
            ]
          }
        })
        .state('app.products.add', {
          url: '/add',
          templateUrl: 'modules/consumer/products/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state,$stateParams, ProductsService,CoreService,FileUploader,Category) {
            this.product = { };
            this.formFields = ProductsService.getFormFields();
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.product.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              ProductsService.upsertProduct(this.product).then(function () {
                $state.go('^.list');
    });
            };
          },
          resolve: {

          }
        })
        .state('app.products.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/consumer/products/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state,$scope, ProductsService, product,FileUploader,CoreService) {
            this.product = product;
            this.formFields = ProductsService.getFormFields();
            this.uploader = new FileUploader({
              url: CoreService.env.apiUrl + 'containers/files/upload',
              formData: [
                {
                  key: 'value'
                }
              ]
            });
            this.submit = function () {
              if (this.uploader.queue[0]){
                this.product.imageSrc = CoreService.env.apiUrl + 'containers/files/download/' +this.uploader.queue[0].file.name;
              }
              ProductsService.upsertProduct(this.product).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            product: function ($stateParams, ProductsService) {
              return ProductsService.getProduct($stateParams.id);
            }
          }
        })
        .state('app.products.view', {
          url: '/:id/view',
          templateUrl: 'modules/consumer/products/views/view.html',
          controllerAs: 'ctrl',
          controller: function (product) {
            this.product = product;
          },
          resolve: {
            product: function ($stateParams, ProductsService) {
              return ProductsService.getProduct($stateParams.id);
            }
          }
        })
        .state('app.products.delete', {
          url: '/:id/delete',
          template: '',
          controllerAs: 'ctrl',
          controller: function ($state, ProductsService, product) {
            ProductsService.deleteProduct(product.id, function () {
              $state.go('^.list');
            }, function () {
              $state.go('^.list');
            });
          },
          resolve: {
            product: function ($stateParams, ProductsService) {
              return ProductsService.getProduct($stateParams.id);
            }
          }
        })
    })
})();
