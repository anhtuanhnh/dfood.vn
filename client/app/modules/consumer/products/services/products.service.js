(function () {
  'use strict';
  angular
    .module('com.module.products')
    .service('ProductsService', function (CoreService,LoopBackAuth, Product,Category, gettextCatalog) {
      var userId = LoopBackAuth.currentUserId;
      console.log(userId);
      this.getProducts = function () {
        return Category.find({
          filter: {
            order: 'created DESC',
            include: [
              'products'
            ],
            where:{
              ownerId: LoopBackAuth.currentUserId
            }
          }
        },function (result) {
          console.log(result)
        }).$promise;
      };

      this.getProduct = function (id) {
        return Product.findById({
          id: id
        }).$promise;
      };

      this.upsertProduct = function (product) {
        console.log(product)
        return Product.upsert(product).$promise
          .then(function () {
            CoreService.toastSuccess(
              gettextCatalog.getString('Cập nhập thành công'),
              gettextCatalog.getString('Thông tin món ăn của bạn đã được cập nhập')
            );
          })
          .catch(function (err) {
            console.log(err)
              CoreService.toastSuccess(
                gettextCatalog.getString('Cập nhập thất bại'),
                gettextCatalog.getString('Cập nhập thông tin món ăn bị lỗi:') + err
              );
            }
          );
      };

      this.deleteProduct = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Bạn muốn xóa món ăn?'),
          gettextCatalog.getString('Xóa sẽ không khôi phục lại được'),
          function () {
            Product.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('Xóa thành công'),
                gettextCatalog.getString('Món ăn của bạn đã được xóa'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Xóa thất bại'),
                gettextCatalog.getString('Xóa món ăn của bạn bị lỗi: ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };
      var optionsCategory = [];
      this.getFormFields = function () {
        var foodTypes = [{
          name: 'Đồ ngọt',
          value: 'Đồ ngọt'
        },{
          name: 'Đồ chay',
          value: 'Đồ chay'
        },
          {
            name: 'Đồ mặn',
            value: 'Đồ mặn'
          },
          {
            name: 'Nguyên liệu',
            value: 'Nguyên liệu'
          }];
        Category.find({
          filter:{
            fields: ['name','id'],
            where: {
              ownerId: userId
            }
          }
        },function (categories) {
          categories.forEach(function(item){
            optionsCategory.push({
              value: item.id,
              name: item.name
            })
          })
        });
        return [
          {
            key: 'name',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên món ăn')
            }
          },
          {
            key: 'categoryId',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Tên nhà hàng'),
              options: optionsCategory
            }
          },
          {
            key: 'introduce',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Giới thiệu')
            }
          },{
            key: 'type',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Loại'),
              options: foodTypes
            }
          },
          {
            key: 'price',
            type: 'input',
            templateOptions: {
              type: 'number',
              label: gettextCatalog.getString('Giá')
            }
          },
          {
            key: 'unit',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Đơn vị '),
              placeholder: 'Kg/g/mlg/cai/combo...'
            }
          },
          {
            key: 'timeProcess',
            type: 'timeAgo',
            templateOptions: {
              label: gettextCatalog.getString('Thời gian chế biến')
            }
          },
          {
            key: 'promotion',
            type: 'input',
            templateOptions: {
              valueDefault: 100,
              label: gettextCatalog.getString('Chương trình khuyến mãi')
            }
          }
        ];
      };
    });

})();
