(function () {
  'use strict';
  angular
    .module('com.module.buyerBookings')
    .config(function ($stateProvider) {
      $stateProvider
        .state('app.buyerBookings', {
          abstract: true,
          url: '/buyerBookings',
          templateUrl: 'modules/buyer/buyerBookings/views/main.html'
        })
        .state('app.buyerBookings.list', {
          url: '',
          templateUrl: 'modules/buyer/buyerBookings/views/list.html',
          controller: 'BuyerBookingCtrl'
        })
        .state('app.buyerBookings.edit', {
          url: '/:id/edit',
          templateUrl: 'modules/buyer/buyerBookings/views/form.html',
          controllerAs: 'ctrl',
          controller: function ($state, BuyerBookingsService, booking) {
            console.log(booking);
            this.booking = booking;
            this.formFields = BuyerBookingsService.getFormFields();
            this.formOptions = {};
            this.submit = function () {
              BuyerBookingsService.upsertBooking(this.booking).then(function () {
                $state.go('^.list');
              });
            };
          },
          resolve: {
            booking: function ($stateParams, BuyerBookingsService) {
              return BuyerBookingsService.getBooking($stateParams.id);
            }
          }
        })
        .state('app.buyerBookings.view', {
          url: '/:id',
          templateUrl: 'modules/buyer/buyerBookings/views/view.html',
          controllerAs: 'ctrl',
          controller: function (booking,BuyerBookingsService,$localStorage) {
            this.booking = booking;
            this.user = $localStorage.user;
            console.log(booking,this.user);
            this.editFoods = function (index,operator,value) {
              if(operator =='plus'){
                this.booking.foods[index].count += 1;
                totalPriceFuc();
              } else if (operator == 'subtract'){
                if(value == 0){
                  console.log('Khong con gi de tru')
                } else {
                  this.booking.foods[index].count -= 1;
                  totalPriceFuc();
                }
              }
            }
             var totalPriceFuc = function () {
               this.booking.totalPrices = 0;
               this.booking.foods.forEach(function (item) {
                 this.booking.totalPrices += item.price * item.count
              }.bind(this));
               BuyerBookingsService.upsertBooking(this.booking)
            }.bind(this)
          },
          resolve: {
            booking: function ($stateParams, BuyerBookingsService) {
              return BuyerBookingsService.getBooking($stateParams.id);
            }
          }
        })
    }
  );

})();
