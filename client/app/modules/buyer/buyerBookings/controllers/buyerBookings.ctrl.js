(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.buyerBookings')
    .controller('BuyerBookingCtrl', function ($scope, $rootScope, Booking) {
      var user = $rootScope.currentUser;
      console.log(user);
      $scope.bookings = {};
      Booking.find({
        filter:{
          where:{
            userId: user.id
          }
        }
      }, function (res) {
        console.log(res);
        $scope.bookings = res;
      })

    });

})();
