(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:MainCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $state

   * @requires CoreService
   * @requires User
   * @requires gettextCatalog
   **/
  angular
    .module('com.module.core')
    .controller('MainCtrl', function ($scope, $rootScope, AppAuth, CoreService, gettextCatalog) {
      $scope.menuoptions = $rootScope.menu;
      $scope.logout = function () {
        AppAuth.logout(function () {
          $rootScope.currentUser = null;
          CoreService.toastSuccess(gettextCatalog.getString('Đăng xuất'),
            gettextCatalog.getString('Bạn đã đăng xuất thành công!'));
        });
      };

    });

})();
