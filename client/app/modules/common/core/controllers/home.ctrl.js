(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.core.controller:HomeCtrl
   * @description Dashboard
   * @requires $scope
   * @requires $rootScope
   **/
  angular
    .module('com.module.core')
    .controller('HomeCtrl', function ($scope, $rootScope) {
      $scope.boxes = $rootScope.dashboardBox;
      var h = window.innerHeight;
      $scope.heightSession = h -101;

    });

})();
