(function () {
  'use strict';
  angular
    .module('com.module.core')
    .service('ApiService', function ($q, $http, ENV, $rootScope,gettextCatalog, User,CoreService, $location,Product, Category, Booking) {

      this.checkRole = function (user,paramUrl) {
          console.log(user);

          // Add Menu Dashboard
          $rootScope.menu = [];

          $rootScope.addMenu(gettextCatalog.getString('Tổng quan'), 'app.home',
            'fa-dashboard');

          // Dashboard
          $rootScope.dashboardBox = [];

          // Add Dashboard Box
          $rootScope.addDashboardBox = function (name, color, icon, quantity, href) {
            $rootScope.dashboardBox.push({
              name: name,
              color: color,
              icon: icon,
              quantity: quantity,
              href: href
            });
          };


          if(user.role == 'admin'){

            $rootScope.addMenu(gettextCatalog.getString('Nhà hàng'), 'app.categories.listCategory', 'fa-file');
            $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.bookings.list', 'fa-edit');
            $rootScope.addMenu(gettextCatalog.getString('Tệp tải lên'), 'app.files.list', 'fa-file');

            $http.get(CoreService.env.apiUrl + '/containers/files/files').success(
              function (data) {
                $rootScope.addDashboardBox(gettextCatalog.getString('Tệp tải lên'), 'bg-blue', 'ion-paperclip', data.length, 'app.files.list');
              });

            Product.find(function (data) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Món ăn'), 'bg-yellow', 'ion-ios7-cart-outline', data.length, 'app.categories.listCategory');
            });

            Category.find(function (data) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Nhà hàng'), 'bg-aqua', 'ion-ios7-pricetag-outline', data.length, 'app.categories.listCategory');
            });

            Booking.find(function (bookings) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.bookings.list');
            });

            $rootScope.addMenu(gettextCatalog.getString('Khách hàng'), 'app.users.list', 'fa-user');
            $location.path(paramUrl);

          } else if(user.role == 'shipper' || user.role == 'user') {

            $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.bookings.list', 'fa-edit');
            Booking.find(function (bookings) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.bookings.list');
            });

          } else if(user.role == 'consumer'){
            $rootScope.addMenu(gettextCatalog.getString('Nhà hàng'), 'app.categories.viewCategory', 'fa-file');
            $rootScope.addMenu(gettextCatalog.getString('Món ăn'), 'app.products.list', 'fa-file');
            $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.bookings.list', 'fa-edit');
            $rootScope.addMenu(gettextCatalog.getString('Hóa đơn'), 'app.invoices.list', 'fa-edit');
            $rootScope.addMenu(gettextCatalog.getString('Cài đặt tài khoản'), 'app.account.list', 'fa-edit');
            Category.find({
              filter:{
                where:{
                  ownerId: $rootScope.currentUser.id
                }
              }
            },function (data) {
              var numberFood = 0;
              data.forEach(function (item) {
                Product.find({
                  filter:{
                    where:{
                      cateforyId: item.id
                    }
                  }
                },function (dataF) {
                  numberFood += dataF.length;
                });
              })
              $rootScope.addDashboardBox(gettextCatalog.getString('Món ăn'), 'bg-yellow', 'ion-ios7-cart-outline', numberFood, 'app.categories.listCategory');

              $rootScope.addDashboardBox(gettextCatalog.getString('Nhà hàng'), 'bg-aqua', 'ion-ios7-pricetag-outline', data.length, 'app.categories.listCategory');
            });

            Booking.find(function (bookings) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.bookings.list');
            });
          }
      };
      this.checkConnection = function () {
        return $q(function (resolve, reject) {
          $http.get(ENV.apiUrl + '/settings')
            .success(resolve)
            .error(reject);
        });
      };

    });

})();
