(function () {
  'use strict';
  angular
    .module('com.module.core')
    .config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
      $locationProvider.html5Mode(true);
      $stateProvider
        .state('app', {
          abstract: true,
          url: '/app',
          templateUrl: 'modules/common/core/views/app.html',
          controller: 'MainCtrl'
        })
        .state('app.home', {
          url: '',
          templateUrl: 'modules/common/core/views/home.html',
          controller: 'HomeCtrl'
        });
      $urlRouterProvider.otherwise('/');
    });

})();
