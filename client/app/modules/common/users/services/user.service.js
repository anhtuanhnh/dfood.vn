(function () {
  'use strict';
  angular
    .module('com.module.users')
    .service('UserService', function ($state, CoreService, User, gettextCatalog,RoleMapping) {

      this.find = function () {
        return User.find().$promise;
      };

      this.findById = function (id) {
        return User.findById({
          id: id
        }).$promise;
      };

      this.upsert = function (user) {
        return User.upsert(user).$promise
          .then(function (res) {
            User.findOne({
              filter:{
                include: 'roles',
                where:{
                  id: res.id
                }
              }
            }, function (a) {
              console.log(a);
              switch (res.role){
                case "user":
                  RoleMapping.upsert({id: a.roles[0].id},{
                    id: a.roles[0].id,
                    principalType: "USER",
                    principalId: res.id,
                    roleId: 2
                  }, function (value) {
                    console.log(value);
                  });
                  break;
                case "consumer":
                  RoleMapping.upsert({id: a.roles[0].id},{
                    id: a.roles[0].id,
                    principalType: "USER",
                    principalId: res.id,
                    roleId: 3
                  }, function (value) {
                    console.log(value);
                  });
                  break;
                case "shipper":
                  RoleMapping.upsert({id: a.roles[0].id},{
                    id: a.roles[0].id,
                    principalType: "USER",
                    principalId: res.id,
                    roleId: 4
                  }, function (value) {
                    console.log(value);
                  });
                  break;
                case "admin":
                  RoleMapping.upsert({id: a.roles[0].id},{
                    id: a.roles[0].id,
                    principalType: "USER",
                    principalId: res.id,
                    roleId: 1
                  }, function (value) {
                    console.log(value);
                  });
                  break;
              }
            })
            console.log(res);


            CoreService.toastSuccess(
              gettextCatalog.getString('User saved'),
              gettextCatalog.getString('Your user is safe with us!')
            );
          })
          .catch(function (err) {
            CoreService.toastError(
              gettextCatalog.getString('Error saving user '),
              gettextCatalog.getString('This user could no be saved: ' + err)
            );
          }
        );
      };

      this.delete = function (id, successCb, cancelCb) {
        CoreService.confirm(
          gettextCatalog.getString('Are you sure?'),
          gettextCatalog.getString('Deleting this cannot be undone'),
          function () {
            User.deleteById({id: id}, function () {
              CoreService.toastSuccess(
                gettextCatalog.getString('User deleted'),
                gettextCatalog.getString('Your user is deleted!'));
              successCb();
            }, function (err) {
              CoreService.toastError(
                gettextCatalog.getString('Error deleting user'),
                gettextCatalog.getString('Your user is not deleted! ') + err);
              cancelCb();
            });
          },
          function () {
            cancelCb();
          }
        );
      };


      this.getFormFields = function (formType) {
        var form = [
          {
            key: 'username',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên hiển thị'),
              required: true
            }
          },
          {
            key: 'fullName',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Họ và tên'),
              required: true
            }
          },{
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại'),
              required: true
            }
          },{
            key: 'address',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Địa chỉ'),
              required: true
            }
          },
          {
            key: 'password',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Mật khẩu'),
              required: true
            }
          }
        ];
        return form;
      };
      this.getFormFieldsAdmin = function (formType) {
        var optionsZone = [{
          name: 'Huế',
          value: 'Huế'
        },{
          name: 'Đà Nẵng',
          value: 'Đà Nẵng'
        },{
          name: 'Quảng Trị',
          value: 'Quảng Trị'
        }];
        var optionsUser = [{
          value: 'admin',
          name: 'admin'
        },{
          value: 'consumer',
          name: 'consumer'
        },{
          value: 'shipper',
          name: 'shipper'
        },{
          value: 'user',
          name: 'user'
        }];
        var form = [
          {
            key: 'username',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên hiển thị'),
              required: true
            }
          },
          {
            key: 'email',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Email'),
              required: true
            }
          },{
            key: 'phone',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Số điện thoại'),
              required: true
            }
          },{
            key: 'address',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Địa chỉ'),
              required: true
            }
          },{
            key: 'role',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Phân quyền'),
              required: true,
              options: optionsUser
            }
          },
          {
            key: 'fullName',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Tên đầy đủ')
            }
          },
          {
            key: 'zone',
            type: 'select',
            templateOptions: {
              label: gettextCatalog.getString('Khu vực làm việc'),
              options: optionsZone
            }
          },
          {
            key: 'password',
            type: 'input',
            templateOptions: {
              label: gettextCatalog.getString('Mật khẩu')
            }
          }
        ];
        return form;
      };
    });

})();
