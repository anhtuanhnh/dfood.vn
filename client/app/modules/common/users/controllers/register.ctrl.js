(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.users.controller:RegisterCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $routeParams
   * @requires $location
   * Controller for Register Page
   **/
  angular
    .module('com.module.users')
    .controller('RegisterCtrl', function ($scope, $routeParams, $location, $filter, CoreService, User, AppAuth, gettextCatalog, RoleMapping) {

      $scope.registration = {
        username: '',
        email: '',
        password: ''
      };

      $scope.schema = [{
        label: '',
        property: 'username',
        placeholder: gettextCatalog.getString('Tên đại diện'),
        type: 'text',
        attr: {
          ngMinlength: 4,
          required: true
        },
        msgs: {
          minlength: gettextCatalog.getString(
            'Ngắn nhất 4 ký tự')
        }
      }, {
        label: '',
        property: 'email',
        placeholder: gettextCatalog.getString('Email'),
        type: 'email',
        help: gettextCatalog.getString(
          'Đừng lo lắng, chúng tôi rất ghét spam tin nhắn.'),
        attr: {
          required: true,
          ngMinlength: 4
        },
        msgs: {
          required: gettextCatalog.getString('Bạn cần một địa chỉ email'),
          email: gettextCatalog.getString('Email của bạn cần ghi chính xác cấu trúc'),
          valid: gettextCatalog.getString('Kiểm tra địa chỉ email thành công!')
        }
      }, {
        type: 'multiple',
        fields: [{
          label: '',
          property: 'password',
          placeholder: gettextCatalog.getString('Mật khẩu'),
          type: 'password',
          attr: {
            required: true,
            ngMinlength: 6
          }
        }, {
          label: '',
          property: 'confirmPassword',
          placeholder: gettextCatalog.getString('Xác nhận mật khẩu'),
          type: 'password',
          attr: {
            confirmPassword: 'user.password',
            required: true,
            ngMinlength: 6
          },
          msgs: {
            match: gettextCatalog.getString(
              'Bạn cần ghi đúng lại mật khẩu')
          }
        }],
        columns: 6
      }];

      $scope.options = {
        validation: {
          enabled: true,
          showMessages: false
        },
        layout: {
          type: 'basic',
          labelSize: 3,
          inputSize: 9
        }
      };

      $scope.confirmPassword = '';

      $scope.register = function () {

        delete $scope.registration.confirmPassword;
        User.save($scope.registration,
          function () {
            $scope.loginResult = User.login({
                include: 'user',
                rememberMe: true
              }, $scope.registration,
              function (res) {
              console.log(res);
                RoleMapping.create({
                  principalType: "USER",
                  "principalId": res.userId,
                  "roleId": 2
                },function (value) {
                  console.log(value)
                });
                AppAuth.currentUser = $scope.loginResult.user;
                CoreService.toastSuccess(gettextCatalog.getString(
                  'Registered'), gettextCatalog.getString(
                  'You are registered!'));
                $location.path('/');
              },
              function (res) {
                CoreService.toastWarning(gettextCatalog.getString(
                  'Error signin in after registration!'), res.data.error
                  .message);
                $scope.loginError = res.data.error;
              }
            );

          },
          function (res) {
            CoreService.toastError(gettextCatalog.getString(
              'Error registering!'), res.data.error.message);
            $scope.registerError = res.data.error;
          }
        );
      };
    })
    .directive('confirmPassword',
    function () {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          var validate = function (viewValue) {
            var password = scope.$eval(attrs.confirmPassword);
            ngModel.$setValidity('match', ngModel.$isEmpty(viewValue) ||
              viewValue === password);
            return viewValue;
          };
          ngModel.$parsers.push(validate);
          scope.$watch(attrs.confirmPassword, function () {
            validate(ngModel.$viewValue);
          });
        }
      };
    }
  );

})();
