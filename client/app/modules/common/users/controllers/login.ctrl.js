(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.users.controller:LoginCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $routeParams
   * @requires $location
   * Contrller for Login Page
   **/
  angular
    .module('com.module.users')
    .controller('LoginCtrl', function ($scope ,$rootScope ,$routeParams ,$state ,$location ,$http ,$localStorage ,CoreService , User, AppAuth, AuthProvider, gettextCatalog,Product,Booking,Category) {
      var userId = localStorage.getItem('$LoopBack$currentUserId');
      var paramUrl = $state.params.url;
      if(userId != null && paramUrl !== '') {
        User.findById({id: userId}, function (res) {
          $rootScope.currentUser = res;
          initDashboardBox(res,paramUrl);
        });
      }
      var TWO_WEEKS = 1000 * 60 * 60 * 24 * 7 * 2;

      $scope.credentials = {
        ttl: TWO_WEEKS,
        rememberMe: true
      };

      if (CoreService.env.name === 'development') {
        $scope.credentials.email = 'admin@admin.com';
        $scope.credentials.password = 'admin';
      }

      $scope.schema = [
        {
          label: '',
          property: 'email',
          placeholder: gettextCatalog.getString('Email'),
          type: 'email',
          attr: {
            required: true,
            ngMinlength: 4
          },
          msgs: {
            required: gettextCatalog.getString('Bạn cần địa chỉ email để đăng nhập'),
            email: gettextCatalog.getString('Email cần đúng định dạng'),
            valid: gettextCatalog.getString('Kiểm tra email thành công!')
          }
        },
        {
          label: '',
          property: 'password',
          placeholder: gettextCatalog.getString('Mật khẩu'),
          type: 'password',
          attr: {
            required: true
          }
        },
        {
          property: 'rememberMe',
          label: gettextCatalog.getString('Lưu tài khoản'),
          type: 'checkbox'
        }
      ];

      $scope.options = {
        validation: {
          enabled: true,
          showMessages: false
        },
        layout: {
          type: 'basic',
          labelSize: 3,
          inputSize: 9
        }
      };

      $scope.socialLogin = function (provider) {
        window.location = CoreService.env.siteUrl + provider.authPath;
      };

      AuthProvider.count(function (result) {
        if (result.count > 0) {
          AuthProvider.find(function (result) {
            $scope.authProviders = result;
          });
        }
      });

      $scope.login = function () {


        $scope.loginResult = User.login({
            include: 'user',
            rememberMe: $scope.credentials.rememberMe
          }, $scope.credentials,
          function (user) {
            console.log(user);

            $rootScope.currentUser = user.user;

            initDashboardBox(user.user);

            AppAuth.currentUser = $scope.loginResult.user;
            CoreService.toastSuccess(gettextCatalog.getString('Logged in'),
              gettextCatalog.getString('You are logged in!'));
            $location.path(paramUrl);
          },
          function (res) {
            $scope.loginError = res.data.error;
          });

      };

      function initDashboardBox(user) {
        console.log(user);

        // Add Menu Dashboard
        $rootScope.menu = [];

        $rootScope.addMenu(gettextCatalog.getString('Tổng quan'), 'app.home',
          'fa-dashboard');

        // Dashboard
        $rootScope.dashboardBox = [];

        // Add Dashboard Box
        $rootScope.addDashboardBox = function (name, color, icon, quantity, href) {
          $rootScope.dashboardBox.push({
            name: name,
            color: color,
            icon: icon,
            quantity: quantity,
            href: href
          });
        };


        if(user.role == 'admin'){

          $rootScope.addMenu(gettextCatalog.getString('Nhà hàng'), 'app.restaurants.list', 'fa-sitemap');
          $rootScope.addMenu(gettextCatalog.getString('Món ăn'), 'app.foods.list', 'fa-cutlery');
          $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.bookings.list', 'fa-paper-plane');
          $rootScope.addMenu(gettextCatalog.getString('Nhân viên giao hàng'), 'app.shippers.list', 'fa-truck');
          $rootScope.addMenu(gettextCatalog.getString('Yêu cầu NV Giao hàng'), 'app.formDriver.list', 'fa-truck');
          $rootScope.addMenu(gettextCatalog.getString('Yêu cầu nhà hàng'), 'app.formRestaurant.list', 'fa-truck');
          $rootScope.addMenu(gettextCatalog.getString('Tài khoảng người dùng'), 'app.users.list', 'fa-users');
          $rootScope.addMenu(gettextCatalog.getString('Tệp tải lên'), 'app.files.list', 'fa-file');
          $rootScope.addMenu(gettextCatalog.getString('Cài đặt'),'app.settings.list', 'fa-cog');
          $http.get(CoreService.env.apiUrl + '/containers/files/files').success(
            function (data) {
              $rootScope.addDashboardBox(gettextCatalog.getString('Tệp tải lên'), 'bg-blue', 'ion-paperclip', data.length, 'app.files.list');
            });

          Product.find(function (data) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Món ăn'), 'bg-yellow', 'ion-ios7-cart-outline', data.length, 'app.categories.listCategory');
          });

          Category.find(function (data) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Nhà hàng'), 'bg-aqua', 'ion-ios7-pricetag-outline', data.length, 'app.categories.listCategory');
          });

          Booking.find(function (bookings) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.bookings.list');
          });

          $location.path(paramUrl);

        } else if(user.role == 'shipper') {

          $rootScope.addMenu(gettextCatalog.getString('Đơn hàng chờ vận chuyển'), 'app.waitBookings.list', 'fa-paper-plane');
          $rootScope.addMenu(gettextCatalog.getString('Đơn hàng đã nhận'), 'app.shipperBookings.list', 'fa-paper-plane');
          Booking.count({
            where:{shipperId : userId}
          },function (bookings) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.count, 'app.shipperBookings.list');
          });
          $location.path(paramUrl);

        } else if(user.role == 'user') {

          $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.buyerBookings.list', 'fa-paper-plane');
          Booking.find({
            filter:{
              where: {
                userId: userId
              }
            }
          },function (bookings) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.buyerBookings.list');
          });
          $location.path(paramUrl);

        } else if(user.role == 'consumer'){
          $rootScope.addMenu(gettextCatalog.getString('Nhà hàng'), 'app.categories.list', 'fa-sitemap');
          $rootScope.addMenu(gettextCatalog.getString('Món ăn'), 'app.products.list', 'fa-cutlery');
          $rootScope.addMenu(gettextCatalog.getString('Đơn hàng'), 'app.consumerBookings.list', 'fa-paper-plane');
          $rootScope.addMenu(gettextCatalog.getString('Hóa đơn'), 'app.invoices.list', 'fa-building');
          $rootScope.addMenu(gettextCatalog.getString('Cài đặt tài khoản'), 'app.account.list', 'fa-cogs');
          Category.find({
            filter:{
              include: 'products',
              where:{
                ownerId: userId
              }
            }
          },function (data) {
            console.log(data)
            var numberFood = 0;
            data.forEach(function (item) {
              numberFood += item.products.length;
            });

            $rootScope.addDashboardBox(gettextCatalog.getString('Món ăn'), 'bg-yellow', 'ion-ios7-cart-outline', numberFood, 'app.categories.listCategory');

            $rootScope.addDashboardBox(gettextCatalog.getString('Nhà hàng'), 'bg-aqua', 'ion-ios7-pricetag-outline', data.length, 'app.categories.listCategory');
          });

          Booking.find(function (bookings) {
            $rootScope.addDashboardBox(gettextCatalog.getString('Đơn hàng'), 'bg-red', 'ion-document-text', bookings.length, 'app.bookings.list');
          });
          $location.path(paramUrl);

        }

      }

    });

})();
