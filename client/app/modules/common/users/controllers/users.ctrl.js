(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name com.module.users.controller:LoginCtrl
   * @description Login Controller
   * @requires $scope
   * @requires $routeParams
   * @requires $location
   * Contrller for Login Page
   **/
  angular
    .module('com.module.users')
    .controller('ManageUserCtrl', function ($scope ,User,uiGridConstants) {

      $scope.dataset = [];

      var loadData = function () {
        User.find({
          filter:{
            order: 'id DESC'
          }
        }, function (res) {
          console.log(res);
          for (var i = 0; i < res.length; i++) {
            var newRow = {
              'id': res[i].id,
              'name': res[i].username,
              'role': res[i].role,
              'phone': res[i].phone,
              'address': res[i].address,
              'created': res[i].created,
            };
            $scope.dataset.push(newRow);
          }
        })

      };

      loadData();

      $scope.gridOptions = {
        showGridFooter: true,
        showColumnFooter: true,
        enableFiltering: true,
        cellEditableCondition: function($scope) {

          // put your enable-edit code here, using values from $scope.row.entity and/or $scope.col.colDef as you desire
          return $scope.row.entity.isActive; // in this example, we'll only allow active rows to be edited

        },

        columnDefs: [
          {
            field: 'id',
            width: '10%',
            displayName: 'Mã NV'
          },
          {
            field: 'name',
            width: '10%',
            displayName: 'Tên NV'
          },
          {
            field: 'role',
            width: '10%',
            displayName: 'Phân quyền'
          },
          {
            field: 'phone',
            aggregationHideLabel: true,
            width: '10%',
            displayName: 'Số điện thoại'
          },
          {
            name: 'address',
            field: 'address',
            width: '13%',
            displayName: 'Địa chỉ'
          },
          {
            name: 'created',
            field: 'created',
            width: '14%',
            displayName: 'Ngày tạo'
          },
          {
            name: 'isActive',
            width: '13%',
            displayName: 'Điều khiển',
            enableColumnMenu: false,
            cellTemplate: '/modules/common/users/views/controlUser.html'

          }
        ],
        data: $scope.dataset,
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;
        }
      };

    });

})();
